SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `exiquetsDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `exiquetsDB` ;

-- -----------------------------------------------------
-- Table `exiquetsDB`.`Categorias`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `exiquetsDB`.`Categorias` (
  `idCategorias` INT NOT NULL ,
  `nombreCategoria` VARCHAR(255) NULL ,
  PRIMARY KEY (`idCategorias`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `exiquetsDB`.`Productos`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `exiquetsDB`.`Productos` (
  `idProductos` INT NOT NULL AUTO_INCREMENT ,
  `descripcion` VARCHAR(255) NULL ,
  `stock` INT NULL ,
  `img` VARCHAR(255) NULL ,
  `precio` FLOAT NULL ,
  `categoria` INT NULL ,
  PRIMARY KEY (`idProductos`) ,
  INDEX `fk_Productos_Categorias1_idx` (`categoria` ASC) ,
  CONSTRAINT `fk_Productos_Categorias1`
    FOREIGN KEY (`categoria` )
    REFERENCES `exiquetsDB`.`Categorias` (`idCategorias` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `exiquetsDB`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `exiquetsDB`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(255) NOT NULL ,
  `mail` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `enabled` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`user_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `exiquetsDB`.`user_roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `exiquetsDB`.`user_roles` (
  `USER_ROLE_ID` INT NOT NULL AUTO_INCREMENT ,
  `USER_ID` INT NOT NULL ,
  `AUTHORITY` VARCHAR(45) NULL ,
  PRIMARY KEY (`USER_ROLE_ID`) ,
  INDEX `FK_user_roles_idx` (`USER_ID` ASC) ,
  CONSTRAINT `FK_user_roles`
    FOREIGN KEY (`USER_ID` )
    REFERENCES `exiquetsDB`.`users` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `exiquetsDB`.`users_has_Productos`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `exiquetsDB`.`users_has_Productos` (
  `users_user_id` INT NOT NULL ,
  `Productos_idProductos` INT NOT NULL ,
  PRIMARY KEY (`users_user_id`, `Productos_idProductos`) ,
  INDEX `fk_users_has_Productos_Productos1_idx` (`Productos_idProductos` ASC) ,
  INDEX `fk_users_has_Productos_users1_idx` (`users_user_id` ASC) ,
  CONSTRAINT `fk_users_has_Productos_users1`
    FOREIGN KEY (`users_user_id` )
    REFERENCES `exiquetsDB`.`users` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_Productos_Productos1`
    FOREIGN KEY (`Productos_idProductos` )
    REFERENCES `exiquetsDB`.`Productos` (`idProductos` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `exiquetsDB` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
