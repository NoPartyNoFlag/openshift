package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.Tipodireccion;


public interface TipoDireccionDAO extends BaseDAOeXiquets<Tipodireccion>  {
}
