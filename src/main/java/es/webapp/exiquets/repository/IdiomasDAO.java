package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.Idioma;


public interface IdiomasDAO extends BaseDAOeXiquets<Idioma>  {
}
