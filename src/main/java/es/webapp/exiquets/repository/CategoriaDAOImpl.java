package es.webapp.exiquets.repository;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.domain.entities.Categoria_;
import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.domain.entities.Idioma_;
import es.webapp.exiquets.domain.entities.Nombrescategoria;
import es.webapp.exiquets.domain.entities.Nombrescategoria_;


@Repository(value = "categoriaDAO")
public class CategoriaDAOImpl extends BaseDAOeXiquetsImpl<Categoria> implements CategoriaDAO {
	public CategoriaDAOImpl(){
		setClazz(Categoria.class);
	}

	@Override
	public Categoria getCategoriaIdiomaSeleccionado(int id) {
		
		String idioma = "es";
		if(FacesContext.getCurrentInstance() != null){
			idioma = FacesContext.getCurrentInstance().getViewRoot().getLocale().toString();
		}
		
		CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Categoria> cq = cb.createQuery(Categoria.class);
		
		Root<Categoria> categoriaRoot = cq.from(Categoria.class);
		
		Fetch<Nombrescategoria,Idioma> idimoaNombreCategoriaFetch = categoriaRoot.fetch(Categoria_.nombrescategorias).fetch(Nombrescategoria_.idioma);
		
		Predicate filters = cb.conjunction();
		filters = cb.and(filters,cb.equal(categoriaRoot.get(Categoria_.idCategorias), id));
		
//		El set de la condicion se puede hacer de dos formas.
//		Forma 1: Haciendo el cast de un fetch a un join.
		filters = cb.and(filters,cb.equal(((Join)idimoaNombreCategoriaFetch).get(Idioma_.idiomaCode), idioma));
		
//		Forma 2: Creando el join y el fetch y añadiendo la condicino al join.
//		Join<Nombrescategoria,Idioma> idiomaNombresCategoriaJoin = categoriaRoot.join(Categoria_.nombrescategorias).join(Nombrescategoria_.idioma);
//		filters = cb.and(filters,cb.equal(idiomaNombresCategoriaJoin.get(Idioma_.locale), locale));
		
		cq.where(filters);

		
		return getCurrentSession().createQuery(cq).getSingleResult();
	}


	@Override
	public Categoria getCategoriaTodosIdiomas(int id) {
		
		CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Categoria> cq = cb.createQuery(Categoria.class);
		
		Root<Categoria> categoriaRoot = cq.from(Categoria.class);
		
		Fetch<Nombrescategoria,Idioma> idimoaNombreCategoriaFetch = categoriaRoot.fetch(Categoria_.nombrescategorias).fetch(Nombrescategoria_.idioma);
		
		Predicate filters = cb.conjunction();
		filters = cb.and(filters,cb.equal(categoriaRoot.get(Categoria_.idCategorias), id));

		
		cq.where(filters);

		
		return getCurrentSession().createQuery(cq).getSingleResult();
	}

	@Override
	public List<Categoria> getCategoriasTodosIdiomas() {
		CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Categoria> cq = cb.createQuery(Categoria.class);
		Root<Categoria> categoriaRoot = cq.from(Categoria.class);
		categoriaRoot.fetch(Categoria_.nombrescategorias).fetch(Nombrescategoria_.idioma);
		cq.distinct(true);
		return getCurrentSession().createQuery(cq).getResultList();
	}
	
	
	
}
