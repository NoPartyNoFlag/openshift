package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.UserRole;

@Repository(value = "rolUsuarioDAO")
public class RolUsuarioDAOImpl extends BaseDAOeXiquetsImpl<UserRole> implements RolUsuarioDAO {
	public RolUsuarioDAOImpl(){
		setClazz(UserRole.class);
	}

}
