package es.webapp.exiquets.repository;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.context.FacesContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.domain.entities.Categoria_;
import es.webapp.exiquets.domain.entities.Descripcion;
import es.webapp.exiquets.domain.entities.Descripcion_;
import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.domain.entities.Idioma_;
import es.webapp.exiquets.domain.entities.Nombre;
import es.webapp.exiquets.domain.entities.Nombre_;
import es.webapp.exiquets.domain.entities.Nombrescategoria;
import es.webapp.exiquets.domain.entities.Nombrescategoria_;
import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.entities.Producto_;

@Repository(value = "productoDAO")
public class ProductosDAOImpl extends BaseDAOeXiquetsImpl<Producto> implements ProductosDAO {
	public ProductosDAOImpl(){
		setClazz(Producto.class);
	}
	
	String idioma = "es";

	@Override
	public List<Producto> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
//		LocaleContextHolder.getLocale(); es_ES
		idioma = FacesContext.getCurrentInstance().getViewRoot().getLocale().toString();
		
		CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Producto> cq = builder.createQuery(Producto.class);
		Root<Producto> productoRoot = cq.from(Producto.class);
		
		Predicate filterCondition = addFilter(builder, productoRoot, cq, filters, sortField, sortOrder);
		
		cq.where(filterCondition);
		
		
		TypedQuery<Producto> query = getCurrentSession().createQuery(cq);
		
		query.setMaxResults(pageSize);
		query.setFirstResult(first);
		
		return query.getResultList();
	}
	
	private Predicate addFilter(CriteriaBuilder builder, Root<Producto> productoRoot, CriteriaQuery<?> cq, Map<String, Object> filters, String sortField, SortOrder sortOrder){

		
		Join<Producto,Nombre> productoNombreJoin = productoRoot.join(Producto_.nombres);
		Join<Nombre,Idioma> nombreIdiomaJoin = productoNombreJoin.join(Nombre_.idioma);
		Join<Producto,Descripcion> productoDescripcionJoin = productoRoot.join(Producto_.descripcions);
		Join<Descripcion,Idioma> descripcionIdiomaJoin = productoDescripcionJoin.join(Descripcion_.idioma);
		Join<Categoria,Nombrescategoria> categoriasNombresCategoriasJoin = productoRoot.join(Producto_.categoriaBean).join(Categoria_.nombrescategorias);
		Join<Nombrescategoria, Idioma> nombresCategoriasIdiomaJoin = categoriasNombresCategoriasJoin.join(Nombrescategoria_.idioma);
		
		//FETCH DE LAS CATEGORIAS
//		productoRoot.fetch(Producto_.categoriaBean).fetch(Categoria_.nombrescategorias);
//		productoRoot.fetch(Producto_.nombres);
//		productoRoot.fetch(Producto_.descripcions).fetch(Descripcion_.literal);
		
//		Sort options
		if(sortField != null && !"".equals(sortField)){
			Path<?> sortPath = null;
			if(sortField.equals("nombre")){
				sortPath = productoNombreJoin.get(Nombre_.literal);
			}else if(sortField.equals("descripcion")){
				sortPath = productoDescripcionJoin.get(Descripcion_.literal);
			}else if(sortField.equals("precio")){
				sortPath = productoRoot.get(Producto_.precio);
			}else if(sortField.equals("stock")){
				sortPath = productoRoot.get(Producto_.stock);
			}else if(sortField.equals("categoria")){
				sortPath = categoriasNombresCategoriasJoin.get(Nombrescategoria_.literalCategoria);
			}
			if(sortPath != null){
				if(sortOrder.equals(SortOrder.ASCENDING)){
					cq.orderBy(builder.asc(sortPath));
				}else if(sortOrder.equals(SortOrder.DESCENDING)){
					cq.orderBy(builder.desc(sortPath));
				}
			}
		}
		
//		WHERE PARA CONDICION DE ID DE USUARIO
		Predicate filterCondition = builder.conjunction();
		
		//Condiciones fijas para el locale
		filterCondition = builder.and(filterCondition,builder.equal(nombreIdiomaJoin.get(Idioma_.idiomaCode), idioma));
		filterCondition = builder.and(filterCondition,builder.equal(descripcionIdiomaJoin.get(Idioma_.idiomaCode), idioma));
		filterCondition = builder.and(filterCondition,builder.equal(nombresCategoriasIdiomaJoin.get(Idioma_.idiomaCode), idioma));
//		filterCondition = builder.and(filterCondition,builder.greaterThan(productoRoot.get(Producto_.stock), 0));
		
		if(filters != null){
			for(Entry<String, Object> filter : filters.entrySet()){
				Predicate p = null;
				if(!"".equals(filter.getValue())){
					if(filter.getKey().equals("nombre")){
						p = builder.like(productoNombreJoin.get(Nombre_.literal), "%" + filter.getValue() + "%");
					}else if(filter.getKey().equals("descripcion")){
						p = builder.like(productoDescripcionJoin.get(Descripcion_.literal), "%" + filter.getValue() + "%");
					}else if(filter.getKey().equals("precio")){
						p = builder.equal(productoRoot.get(Producto_.precio), filter.getValue());
					}else if(filter.getKey().equals("precioMenor")){
						p = builder.greaterThanOrEqualTo(productoRoot.get(Producto_.precio), (Float)filter.getValue());
					}else if(filter.getKey().equals("precioMayor")){
						p = builder.lessThanOrEqualTo(productoRoot.get(Producto_.precio), (Float)filter.getValue());
					}else if(filter.getKey().equals("stock")){
						p = builder.equal(productoRoot.get(Producto_.stock), filter.getValue());
					}else if(filter.getKey().equals("categoria")){
						p = builder.equal(productoRoot.get(Producto_.categoriaBean).get(Categoria_.idCategorias), filter.getValue());
					}
				}
				if(p != null){
					filterCondition = builder.and(filterCondition, p);	
				}
			}
		}
		return filterCondition;
	}
	
	

	@Override
	public int count(Map<String, Object> filters) {
		CriteriaBuilder builder = 	getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
		
		Root<Producto> productoRoot = criteriaQuery.from(Producto.class);
		
		Predicate filterCondition = addFilter(builder, productoRoot, criteriaQuery, filters, null, null);
		criteriaQuery.where(filterCondition);

		criteriaQuery.select(builder.count(productoRoot));
		
		return getCurrentSession().createQuery(criteriaQuery).getSingleResult().intValue();
	}

	@Override
	public Producto getProductoTodosIdiomas(int id) {
		CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Producto> cq = cb.createQuery(Producto.class);
		
		Root<Producto> productoRoot = cq.from(Producto.class);
		
		productoRoot.fetch(Producto_.nombres).fetch(Nombre_.idioma);
		productoRoot.fetch(Producto_.descripcions).fetch(Descripcion_.idioma);
		productoRoot.fetch(Producto_.categoriaBean).fetch(Categoria_.nombrescategorias).fetch(Nombrescategoria_.idioma);
		
		Predicate filters = cb.conjunction();
		filters = cb.and(filters, cb.equal(productoRoot.get(Producto_.idProductos), id));
		
		
		cq.where(filters);
		
		cq.distinct(true);
		
		return getCurrentSession().createQuery(cq).getSingleResult();
	}

	@Override
	public List<Producto> getProductosTodosIdiomas() {
		CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Producto> cq = cb.createQuery(Producto.class);
		
		Root<Producto> productoRoot = cq.from(Producto.class);
		
		productoRoot.fetch(Producto_.nombres).fetch(Nombre_.idioma);
		productoRoot.fetch(Producto_.descripcions).fetch(Descripcion_.idioma);
		productoRoot.fetch(Producto_.categoriaBean).fetch(Categoria_.nombrescategorias).fetch(Nombrescategoria_.idioma);
		
		cq.distinct(true);
		
		return getCurrentSession().createQuery(cq).getResultList();
	}
	
	

}
