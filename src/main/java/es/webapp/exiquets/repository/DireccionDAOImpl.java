package es.webapp.exiquets.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Direccion;
import es.webapp.exiquets.domain.entities.Direccion_;
import es.webapp.exiquets.domain.entities.User_;

@Repository(value = "direccionDAO")
public class DireccionDAOImpl extends BaseDAOeXiquetsImpl<Direccion> implements DireccionDAO {
	public DireccionDAOImpl(){
		setClazz(Direccion.class);
	}

	@Override
	public List<Direccion> getDireccionesFiltradas(Direccion filtro) {
		CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Direccion> cq = cb.createQuery(Direccion.class);
		Root<Direccion> direccionRoot = cq.from(Direccion.class);
		
		direccionRoot.fetch(Direccion_.user);
		
		if(filtro != null){
			Predicate predicado = cb.conjunction();
			if(filtro.getUser() != null){
				predicado = cb.and(predicado, cb.equal(direccionRoot.get(Direccion_.user).get(User_.userId), filtro.getUser().getUserId()));
			}
			if(filtro.getActiva() == 1){
				predicado = cb.and(predicado, cb.equal(direccionRoot.get(Direccion_.activa), filtro.getActiva()));
			}
			if(filtro.getDefecto() == 1){
				predicado = cb.and(predicado, cb.equal(direccionRoot.get(Direccion_.defecto), filtro.getDefecto()));
			}
			cq.where(predicado);
		}
		
		return getCurrentSession().createQuery(cq).getResultList();
	}

}
