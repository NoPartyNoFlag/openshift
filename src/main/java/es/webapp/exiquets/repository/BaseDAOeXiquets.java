package es.webapp.exiquets.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

public interface BaseDAOeXiquets<T extends Serializable> {

	public void setClazz(final Class<T> clazzToSet);
	
	public T findOne(final Integer id);
	
	public List<T> findAll();
	
	public void save(final T entity);
	
	public void update(final T entity);
	
	public void delete(final T entity);
	
	public void deleteById(final Integer entityId);
	
	public EntityManager getCurrentSession();
	
	
}
