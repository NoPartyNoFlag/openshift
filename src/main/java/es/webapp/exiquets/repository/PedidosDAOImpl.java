package es.webapp.exiquets.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.EstadoPedido_;
import es.webapp.exiquets.domain.entities.Pedido;
import es.webapp.exiquets.domain.entities.Pedido_;
import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.User_;


@Repository(value = "pedidosDAO")
public class PedidosDAOImpl extends BaseDAOeXiquetsImpl<Pedido> implements PedidosDAO {
	public PedidosDAOImpl(){
		setClazz(Pedido.class);
	}
	
	
	@Override
	public int count(Map<String, Object> filters) {
		CriteriaBuilder builder = 	getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
		
		Root<Pedido> productoRoot = criteriaQuery.from(Pedido.class);
		
		Predicate filterCondition = addFilter(builder, productoRoot, criteriaQuery, filters, null, null, true);
		criteriaQuery.where(filterCondition);

		criteriaQuery.select(builder.count(productoRoot));
		
		return getCurrentSession().createQuery(criteriaQuery).getSingleResult().intValue();
	}
	
	private Predicate addFilter(CriteriaBuilder builder, Root<Pedido> pedidoRoot, CriteriaQuery<?> cq, Map<String, Object> filters, String sortField, SortOrder sortOrder, boolean isCount){
	
		Join<Pedido,User> pedidoUserJoin = pedidoRoot.join(Pedido_.user);
		if(!isCount){
			pedidoRoot.fetch(Pedido_.estadoPedido);
		}
		
//		Sort options
		if(sortField != null && !"".equals(sortField)){
			Path<?> sortPath = null;
			if(sortField.equals("nombreUsuario")){
				sortPath = pedidoUserJoin.get(User_.nombre);
			}else if(sortField.equals("fechaPedido")){
				sortPath = pedidoRoot.get(Pedido_.fechaPedido);
			}else if(sortField.equals("estadoPedido")){
				sortPath = pedidoRoot.get(Pedido_.estadoPedido);
			}
			if(sortPath != null){
				if(sortOrder.equals(SortOrder.ASCENDING)){
					cq.orderBy(builder.asc(sortPath));
				}else if(sortOrder.equals(SortOrder.DESCENDING)){
					cq.orderBy(builder.desc(sortPath));
				}
			}
		}
		
//		WHERE PARA CONDICION DE ID DE USUARIO
		Predicate filterCondition = builder.conjunction();
		
		if(filters != null){
			for(Entry<String, Object> filter : filters.entrySet()){
				Predicate p = null;
				if(!"".equals(filter.getValue())){
					if(filter.getKey().equals("nombreUsuario")){
						p = builder.like(pedidoUserJoin.get(User_.nombre), "%" + filter.getValue() + "%");
					}else if(filter.getKey().equals("userId")){
						p = builder.equal(pedidoUserJoin.get(User_.userId), filter.getValue());
					}else if(filter.getKey().equals("from")){
						p = builder.greaterThanOrEqualTo(pedidoRoot.get(Pedido_.fechaPedido), (Date)filter.getValue());
					}else if(filter.getKey().equals("to")){
						p = builder.lessThanOrEqualTo(pedidoRoot.get(Pedido_.fechaPedido), (Date)filter.getValue());
					}else if(filter.getKey().equals("estadoPedido")){
						p = builder.equal(pedidoRoot.get(Pedido_.estadoPedido).get(EstadoPedido_.idEstadoPedido), filter.getValue());
					}
				}
				if(p != null){
					filterCondition = builder.and(filterCondition, p);	
				}
			}
		}
		return filterCondition;
	}	

	@Override
	public List<Pedido> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
		CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Pedido> cq = builder.createQuery(Pedido.class);
		Root<Pedido> pedidoRoot = cq.from(Pedido.class);
		
		Predicate filterCondition = addFilter(builder, pedidoRoot, cq, filters, sortField, sortOrder, false);
		
		cq.where(filterCondition);
		
		
		TypedQuery<Pedido> query = getCurrentSession().createQuery(cq);
		
		query.setMaxResults(pageSize);
		query.setFirstResult(first);
		
		return query.getResultList();
	}


}
