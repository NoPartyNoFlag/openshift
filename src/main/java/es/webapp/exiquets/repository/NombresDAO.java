package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.Nombre;


public interface NombresDAO extends BaseDAOeXiquets<Nombre>  {
}
