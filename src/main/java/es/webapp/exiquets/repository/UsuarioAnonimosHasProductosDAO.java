package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProducto;


public interface UsuarioAnonimosHasProductosDAO extends BaseDAOeXiquets<UsuarioanonimosHasProducto>  {

}
