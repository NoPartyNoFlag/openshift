package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Nombre;

@Repository(value = "nombresDAO")
public class NombresDAOImpl extends BaseDAOeXiquetsImpl<Nombre> implements NombresDAO {
	public NombresDAOImpl(){
		setClazz(Nombre.class);
	}

}
