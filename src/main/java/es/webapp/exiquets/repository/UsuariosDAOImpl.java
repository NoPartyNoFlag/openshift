package es.webapp.exiquets.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.User_;

@Repository(value = "usuarioDAO")
public class UsuariosDAOImpl extends BaseDAOeXiquetsImpl<User> implements UsuariosDAO {
	public UsuariosDAOImpl(){
		setClazz(User.class);
	}


	@Override
	public User getUsuarioFiltrado(User user) {
    	CriteriaBuilder cb = getCurrentSession().getCriteriaBuilder();
    	CriteriaQuery<User> cq = cb.createQuery(User.class);
    	Root<User> userRoot = cq.from(User.class);
    	
    	userRoot.fetch(User_.userRoles);
    	Predicate predicate = cb.conjunction();
    	if(user.getUserId() != 0){
    		predicate = cb.and(predicate, cb.equal(userRoot.get(User_.userId), user.getUserId()));
    	}
    	if(user.getMail() != null && !"".equals(user.getMail())){
    		predicate = cb.and(predicate, cb.equal(userRoot.get(User_.mail), user.getMail()));
    	}
    	if(user.getEnabled() != 0){
    		predicate = cb.and(predicate, cb.equal(userRoot.get(User_.enabled), user.getEnabled()));
    	}
    	
    	cq.where(predicate);
    	
    	return getCurrentSession().createQuery(cq).getSingleResult();
	}

}
