package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.Usuariosanonimo;
import es.webapp.exiquets.domain.webforms.UsuarioWeb;


public interface UsuariosAnonimosDAO extends BaseDAOeXiquets<Usuariosanonimo>  {
	public Usuariosanonimo getUsuarioPorSesion(String sesion);
}
