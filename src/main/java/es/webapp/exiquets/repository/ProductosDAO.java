package es.webapp.exiquets.repository;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import es.webapp.exiquets.domain.entities.Producto;


public interface ProductosDAO extends BaseDAOeXiquets<Producto>  {

	public List<Producto> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters);
	public int count(Map<String,Object> filters);
	public Producto getProductoTodosIdiomas(int id);
	public List<Producto> getProductosTodosIdiomas();
}
