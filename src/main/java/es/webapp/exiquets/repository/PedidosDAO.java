package es.webapp.exiquets.repository;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import es.webapp.exiquets.domain.entities.Pedido;


public interface PedidosDAO extends BaseDAOeXiquets<Pedido>  {

	public int count(Map<String,Object> filters);
    public List<Pedido> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters);
}
