package es.webapp.exiquets.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Categoria_;
import es.webapp.exiquets.domain.entities.Descripcion_;
import es.webapp.exiquets.domain.entities.Nombre_;
import es.webapp.exiquets.domain.entities.Nombrescategoria_;
import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.entities.Producto_;
import es.webapp.exiquets.domain.entities.UsersHasProducto;
import es.webapp.exiquets.domain.entities.UsersHasProductoPK_;
import es.webapp.exiquets.domain.entities.UsersHasProducto_;

@Repository(value = "usersHasProductosDAO")
public class UsersHasProductosDAOImpl extends BaseDAOeXiquetsImpl<UsersHasProducto> implements UsersHasProductosDAO {
	public UsersHasProductosDAOImpl(){
		setClazz(UsersHasProducto.class);
	}

	@Override
	public List<UsersHasProducto> getUsersHasProductoCompleto(UsersHasProducto filtro) {
		CriteriaBuilder builder = 	getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<UsersHasProducto> criteriaQuery = builder.createQuery(UsersHasProducto.class);
		
		Root<UsersHasProducto> root = criteriaQuery.from(UsersHasProducto.class);
		
		Fetch<UsersHasProducto, Producto> productoFetch = root.fetch(UsersHasProducto_.producto);
		productoFetch.fetch(Producto_.categoriaBean).fetch(Categoria_.nombrescategorias).fetch(Nombrescategoria_.idioma);
		productoFetch.fetch(Producto_.nombres).fetch(Nombre_.idioma);
		productoFetch.fetch(Producto_.descripcions).fetch(Descripcion_.idioma);
		
		Predicate predicate = builder.conjunction();
		if(filtro.getId() != null){
			if(filtro.getId().getProductos_idProductos() > 0){
				predicate = builder.and(builder.equal(root.get(UsersHasProducto_.id).get(UsersHasProductoPK_.productos_idProductos), filtro.getId().getProductos_idProductos()));	
			}
			if(filtro.getId().getUsersUserId() > 0){
				predicate = builder.and(predicate,builder.equal(root.get(UsersHasProducto_.id).get(UsersHasProductoPK_.usersUserId), filtro.getId().getUsersUserId()));	
			}
		}

		criteriaQuery.distinct(true);
		criteriaQuery.where(predicate);
		
		return getCurrentSession().createQuery(criteriaQuery).getResultList();
	}

}
