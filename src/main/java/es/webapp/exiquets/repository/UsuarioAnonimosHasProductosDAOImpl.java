package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProducto;

@Repository(value = "usuarioAnonimosHasProductosDAO")
public class UsuarioAnonimosHasProductosDAOImpl extends BaseDAOeXiquetsImpl<UsuarioanonimosHasProducto> implements UsuarioAnonimosHasProductosDAO {
	public UsuarioAnonimosHasProductosDAOImpl(){
		setClazz(UsuarioanonimosHasProducto.class);
	}

}
