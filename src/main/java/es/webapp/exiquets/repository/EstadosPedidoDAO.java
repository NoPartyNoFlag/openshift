package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.EstadoPedido;


public interface EstadosPedidoDAO extends BaseDAOeXiquets<EstadoPedido>  {
}
