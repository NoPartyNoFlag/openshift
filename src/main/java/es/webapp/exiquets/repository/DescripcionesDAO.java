package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.Descripcion;


public interface DescripcionesDAO extends BaseDAOeXiquets<Descripcion>  {
}
