package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.User;


public interface UsuariosDAO extends BaseDAOeXiquets<User>  {
	public User getUsuarioFiltrado(User u);
}
