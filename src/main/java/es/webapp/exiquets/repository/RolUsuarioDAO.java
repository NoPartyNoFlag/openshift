package es.webapp.exiquets.repository;

import es.webapp.exiquets.domain.entities.UserRole;


public interface RolUsuarioDAO extends BaseDAOeXiquets<UserRole>  {
}
