package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.EstadoPedido;

@Repository(value = "estadosPedidoDAO")
public class EstadosPedidoDAOImpl extends BaseDAOeXiquetsImpl<EstadoPedido> implements EstadosPedidoDAO {
	public EstadosPedidoDAOImpl(){
		setClazz(EstadoPedido.class);
	}

}
