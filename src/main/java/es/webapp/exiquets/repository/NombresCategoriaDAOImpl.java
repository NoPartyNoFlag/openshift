package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Nombrescategoria;


@Repository(value = "nombresCategoriaDAO")
public class NombresCategoriaDAOImpl extends BaseDAOeXiquetsImpl<Nombrescategoria> implements NombresCategoriaDAO {
	public NombresCategoriaDAOImpl(){
		setClazz(Nombrescategoria.class);
	}

}
