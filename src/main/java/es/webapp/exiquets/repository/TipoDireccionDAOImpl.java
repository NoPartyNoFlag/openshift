package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Tipodireccion;

@Repository(value = "tipoDireccionDAO")
public class TipoDireccionDAOImpl extends BaseDAOeXiquetsImpl<Tipodireccion> implements TipoDireccionDAO {
	public TipoDireccionDAOImpl(){
		setClazz(Tipodireccion.class);
	}

}
