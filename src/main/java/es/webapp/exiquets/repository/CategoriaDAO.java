package es.webapp.exiquets.repository;

import java.util.List;

import es.webapp.exiquets.domain.entities.Categoria;


public interface CategoriaDAO extends BaseDAOeXiquets<Categoria>  {

	public Categoria getCategoriaIdiomaSeleccionado(int id);
	public Categoria getCategoriaTodosIdiomas(int id);
	public List<Categoria> getCategoriasTodosIdiomas();
}
