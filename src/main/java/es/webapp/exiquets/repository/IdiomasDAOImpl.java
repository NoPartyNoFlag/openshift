package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Idioma;

@Repository(value = "idiomasDAO")
public class IdiomasDAOImpl extends BaseDAOeXiquetsImpl<Idioma> implements IdiomasDAO {
	public IdiomasDAOImpl(){
		setClazz(Idioma.class);
	}

}
