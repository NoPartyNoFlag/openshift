package es.webapp.exiquets.repository;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Descripcion;

@Repository(value = "descripcionesDAO")
public class DescripcionesDAOImpl extends BaseDAOeXiquetsImpl<Descripcion> implements DescripcionesDAO {
	public DescripcionesDAOImpl(){
		setClazz(Descripcion.class);
	}

}
