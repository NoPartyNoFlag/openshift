package es.webapp.exiquets.repository;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Categoria_;
import es.webapp.exiquets.domain.entities.Descripcion;
import es.webapp.exiquets.domain.entities.Descripcion_;
import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.domain.entities.Idioma_;
import es.webapp.exiquets.domain.entities.Nombre;
import es.webapp.exiquets.domain.entities.Nombre_;
import es.webapp.exiquets.domain.entities.Nombrescategoria;
import es.webapp.exiquets.domain.entities.Nombrescategoria_;
import es.webapp.exiquets.domain.entities.PedidoHasProducto;
import es.webapp.exiquets.domain.entities.PedidoHasProductoPK_;
import es.webapp.exiquets.domain.entities.PedidoHasProducto_;
import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.entities.Producto_;

@Repository(value = "pedidosHasProductosDAO")
public class PedidosHasProductosDAOImpl extends BaseDAOeXiquetsImpl<PedidoHasProducto> implements PedidosHasProductosDAO {
	public PedidosHasProductosDAOImpl(){
		setClazz(PedidoHasProducto.class);
	}

	@Override
	public List<PedidoHasProducto> getPedidoHasProductoCompletoById(int id) {
		
		String locale = "es";
		
		if(FacesContext.getCurrentInstance() != null){
			locale = FacesContext.getCurrentInstance().getViewRoot().getLocale().toString();
		}
		
		CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<PedidoHasProducto> cq = builder.createQuery(PedidoHasProducto.class);
		Root<PedidoHasProducto> pedidoHasProductoRoot = cq.from(PedidoHasProducto.class);
		
		Fetch<PedidoHasProducto,Producto> fetchPedidoProducto = pedidoHasProductoRoot.fetch(PedidoHasProducto_.producto);
		
		fetchPedidoProducto.fetch(Producto_.categoriaBean).fetch(Categoria_.nombrescategorias).fetch(Nombrescategoria_.idioma);
		fetchPedidoProducto.fetch(Producto_.nombres).fetch(Nombre_.idioma);
		fetchPedidoProducto.fetch(Producto_.descripcions).fetch(Descripcion_.idioma);
		
		Predicate filterCondition = builder.conjunction();
		filterCondition = builder.and(filterCondition,builder.equal(pedidoHasProductoRoot.get(PedidoHasProducto_.id).get(PedidoHasProductoPK_.pedido_idPedido), id));

		
		cq.where(filterCondition).distinct(true);
		
		
		TypedQuery<PedidoHasProducto> query = getCurrentSession().createQuery(cq);
		
		
		return query.getResultList();
	}

}
