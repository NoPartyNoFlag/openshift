package es.webapp.exiquets.repository;

import java.util.List;

import es.webapp.exiquets.domain.entities.Direccion;


public interface DireccionDAO extends BaseDAOeXiquets<Direccion>  {
	
	public List<Direccion> getDireccionesFiltradas(Direccion filtro);
}
