package es.webapp.exiquets.repository;

import java.util.List;

import es.webapp.exiquets.domain.entities.PedidoHasProducto;


public interface PedidosHasProductosDAO extends BaseDAOeXiquets<PedidoHasProducto>  {

	public List<PedidoHasProducto> getPedidoHasProductoCompletoById(int id);
}
