package es.webapp.exiquets.repository;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import es.webapp.exiquets.domain.entities.Usuariosanonimo;

@Repository(value = "usuarioAnonimoDAO")
public class UsuariosAnonimosDAOImpl extends BaseDAOeXiquetsImpl<Usuariosanonimo> implements UsuariosAnonimosDAO {
	public UsuariosAnonimosDAOImpl(){
		setClazz(Usuariosanonimo.class);
	}

	public Usuariosanonimo getUsuarioPorSesion(String sesion) {
//		return (Usuariosanonimo) getCurrentSession().createQuery("from "+ Usuariosanonimo.class.getName() +" as u WHERE u.idSesion like :sesion");
		Usuariosanonimo user = null;
		Query q = getCurrentSession().createQuery("from "+ Usuariosanonimo.class.getName() + " as u WHERE u.idSesion like :sesion",Usuariosanonimo.class);
		q.setParameter("sesion", sesion);
		List<Usuariosanonimo> usuarios = q.getResultList();
		if(usuarios.size() != 0){
			user = usuarios.get(0);
		}
		return user;
	}


}
