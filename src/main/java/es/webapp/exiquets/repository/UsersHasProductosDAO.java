package es.webapp.exiquets.repository;

import java.util.List;

import es.webapp.exiquets.domain.entities.UsersHasProducto;


public interface UsersHasProductosDAO extends BaseDAOeXiquets<UsersHasProducto>  {
	public List<UsersHasProducto> getUsersHasProductoCompleto(UsersHasProducto filtro);
}
