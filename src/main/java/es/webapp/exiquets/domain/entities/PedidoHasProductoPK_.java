package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.883+0200")
@StaticMetamodel(PedidoHasProductoPK.class)
public class PedidoHasProductoPK_ {
	public static volatile SingularAttribute<PedidoHasProductoPK, Integer> pedido_idPedido;
	public static volatile SingularAttribute<PedidoHasProductoPK, Integer> productos_idProductos;
}
