package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.743+0200")
@StaticMetamodel(Descripcion.class)
public class Descripcion_ {
	public static volatile SingularAttribute<Descripcion, Integer> idDescripcion;
	public static volatile SingularAttribute<Descripcion, String> literal;
	public static volatile SingularAttribute<Descripcion, Idioma> idioma;
	public static volatile SetAttribute<Descripcion, Producto> productos;
}
