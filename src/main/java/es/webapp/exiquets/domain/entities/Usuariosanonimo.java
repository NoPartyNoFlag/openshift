package es.webapp.exiquets.domain.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * The persistent class for the usuariosanonimos database table.
 * 
 */
@Entity
@Table(name="usuariosanonimos")
@NamedQuery(name="Usuariosanonimo.findAll", query="SELECT u FROM Usuariosanonimo u")
public class Usuariosanonimo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idUsuarioAnonimo;

	@Column(length=255)
	private String idSesion;

	//bi-directional many-to-one association to UsuarioanonimosHasProducto
	@OneToMany(mappedBy="usuariosanonimo")
	private Set<UsuarioanonimosHasProducto> usuarioanonimosHasProductos = new HashSet<UsuarioanonimosHasProducto>();

	//bi-directional many-to-many association to Producto
	@ManyToMany
	@JoinTable(
		name="usuarioanonimos_has_productos"
		, joinColumns={
			@JoinColumn(name="usuariosAnonimos_idUsuarioAnonimo", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="productos_idProductos", nullable=false)
			}
		)
	private Set<Producto> productos = new HashSet<Producto>();
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion = new Date();

	public Usuariosanonimo() {
	}

	public int getIdUsuarioAnonimo() {
		return this.idUsuarioAnonimo;
	}

	public void setIdUsuarioAnonimo(int idUsuarioAnonimo) {
		this.idUsuarioAnonimo = idUsuarioAnonimo;
	}

	public String getIdSesion() {
		return this.idSesion;
	}

	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}

	public Set<UsuarioanonimosHasProducto> getUsuarioanonimosHasProductos() {
		return this.usuarioanonimosHasProductos;
	}

	public void setUsuarioanonimosHasProductos(Set<UsuarioanonimosHasProducto> usuarioanonimosHasProductos) {
		this.usuarioanonimosHasProductos = usuarioanonimosHasProductos;
	}

	public UsuarioanonimosHasProducto addUsuarioanonimosHasProducto(UsuarioanonimosHasProducto usuarioanonimosHasProducto) {
		getUsuarioanonimosHasProductos().add(usuarioanonimosHasProducto);
		usuarioanonimosHasProducto.setUsuariosanonimo(this);

		return usuarioanonimosHasProducto;
	}

	public UsuarioanonimosHasProducto removeUsuarioanonimosHasProducto(UsuarioanonimosHasProducto usuarioanonimosHasProducto) {
		getUsuarioanonimosHasProductos().remove(usuarioanonimosHasProducto);
		usuarioanonimosHasProducto.setUsuariosanonimo(null);

		return usuarioanonimosHasProducto;
	}

	public Set<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(Set<Producto> productos) {
		this.productos = productos;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public int hashCode() {
	    return idUsuarioAnonimo;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Usuariosanonimo && this.idUsuarioAnonimo == ((Usuariosanonimo)object).idUsuarioAnonimo) {
        	isEqual = true;
        }

        return isEqual;
	}
}