package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the pedido_has_productos database table.
 * 
 */
@Embeddable
public class PedidoHasProductoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int pedido_idPedido;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int productos_idProductos;

	public PedidoHasProductoPK() {
	}
	public int getPedido_idPedido() {
		return this.pedido_idPedido;
	}
	public void setPedido_idPedido(int pedido_idPedido) {
		this.pedido_idPedido = pedido_idPedido;
	}
	public int getProductos_idProductos() {
		return this.productos_idProductos;
	}
	public void setProductos_idProductos(int productos_idProductos) {
		this.productos_idProductos = productos_idProductos;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PedidoHasProductoPK)) {
			return false;
		}
		PedidoHasProductoPK castOther = (PedidoHasProductoPK)other;
		return 
			(this.pedido_idPedido == castOther.pedido_idPedido)
			&& (this.productos_idProductos == castOther.productos_idProductos);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.pedido_idPedido;
		hash = hash * prime + this.productos_idProductos;
		
		return hash;
	}
}