package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;


/**
 * The persistent class for the categorias database table.
 * 
 */
@Entity
@Table(name="categorias")
@NamedQuery(name="Categoria.findAll", query="SELECT c FROM Categoria c")
public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idCategorias;

	//bi-directional many-to-many association to Nombrescategoria
	@ManyToMany(mappedBy="categorias")
	private Set<Nombrescategoria> nombrescategorias = new HashSet<Nombrescategoria>();

	//bi-directional many-to-one association to Producto
	@OneToMany(mappedBy="categoriaBean")
	private Set<Producto> productos;

	public Categoria() {
	}

	public int getIdCategorias() {
		return this.idCategorias;
	}

	public void setIdCategorias(int idCategorias) {
		this.idCategorias = idCategorias;
	}

	public Set<Nombrescategoria> getNombrescategorias() {
		return this.nombrescategorias;
	}

	public void setNombrescategorias(Set<Nombrescategoria> nombrescategorias) {
		this.nombrescategorias = nombrescategorias;
	}

	public Set<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(Set<Producto> productos) {
		this.productos = productos;
	}

	public Producto addProducto(Producto producto) {
		getProductos().add(producto);
		producto.setCategoriaBean(this);

		return producto;
	}

	public Producto removeProducto(Producto producto) {
		getProductos().remove(producto);
		producto.setCategoriaBean(null);

		return producto;
	}
	@Override
	public int hashCode() {
	    return idCategorias;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Categoria && this.idCategorias == ((Categoria)object).idCategorias) {
        	isEqual = true;
        }

        return isEqual;
    }
	
	public String getLiteralNombre(String locale){
		String nombre = "No hay literal en " + locale;
		
		for(Nombrescategoria n :nombrescategorias){
			if(n.getIdioma().getIdiomaCode().equals(locale)){
				nombre = n.getLiteralCategoria();
			}
		}
		return nombre;
	}
}