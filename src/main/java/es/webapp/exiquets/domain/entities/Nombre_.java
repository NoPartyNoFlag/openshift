package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.821+0200")
@StaticMetamodel(Nombre.class)
public class Nombre_ {
	public static volatile SingularAttribute<Nombre, Integer> idNombre;
	public static volatile SingularAttribute<Nombre, String> literal;
	public static volatile SingularAttribute<Nombre, Idioma> idioma;
	public static volatile SetAttribute<Nombre, Producto> productos;
}
