package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.852+0200")
@StaticMetamodel(Nombrescategoria.class)
public class Nombrescategoria_ {
	public static volatile SingularAttribute<Nombrescategoria, Integer> idNombresCategorias;
	public static volatile SingularAttribute<Nombrescategoria, String> literalCategoria;
	public static volatile SetAttribute<Nombrescategoria, Categoria> categorias;
	public static volatile SingularAttribute<Nombrescategoria, Idioma> idioma;
}
