package es.webapp.exiquets.domain.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the pedido database table.
 * 
 */
@Entity
@Table(name="pedido")
@NamedQuery(name="Pedido.findAll", query="SELECT p FROM Pedido p")
public class Pedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idPedido;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaPedido;
	
	//bi-directional many-to-one association to Direccion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="direccion_idDireccion", nullable=false)
	private Direccion direccion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="estadoPedido", nullable=false)
	private EstadoPedido estadoPedido;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="userId", nullable=false)
	private User user;

	//bi-directional many-to-one association to PedidoHasProducto
	@OneToMany(mappedBy="pedido")
	private Set<PedidoHasProducto> pedidoHasProductos;

	//bi-directional many-to-many association to Producto
	@ManyToMany(mappedBy="pedidos")
	private Set<Producto> productos;

	public Pedido() {
	}

	public int getIdPedido() {
		return this.idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public EstadoPedido getEstadoPedido() {
		return this.estadoPedido;
	}

	public void setEstadoPedido(EstadoPedido estadoPedido) {
		this.estadoPedido = estadoPedido;
	}

	public Date getFechaPedido() {
		return this.fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}
	
	public Direccion getDireccion() {
		return this.direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<PedidoHasProducto> getPedidoHasProductos() {
		return this.pedidoHasProductos;
	}

	public void setPedidoHasProductos(Set<PedidoHasProducto> pedidoHasProductos) {
		this.pedidoHasProductos = pedidoHasProductos;
	}

	public PedidoHasProducto addPedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().add(pedidoHasProducto);
		pedidoHasProducto.setPedido(this);

		return pedidoHasProducto;
	}

	public PedidoHasProducto removePedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().remove(pedidoHasProducto);
		pedidoHasProducto.setPedido(null);

		return pedidoHasProducto;
	}

	public Set<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(Set<Producto> productos) {
		this.productos = productos;
	}

	@Override
	public int hashCode() {
	    return idPedido;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Pedido && this.idPedido == ((Pedido)object).idPedido) {
        	isEqual = true;
        }
        return isEqual;
    }
}