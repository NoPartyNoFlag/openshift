package es.webapp.exiquets.domain.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Set;


/**
 * The persistent class for the direccion database table.
 * 
 */
@Entity
@Table(name="direccion")
@NamedQuery(name="Direccion.findAll", query="SELECT d FROM Direccion d")
public class Direccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idDireccion;
	
	@Column(length=255)
	private String nombreDireccion;

	@Column(length=45)
	private String ciudad;

	private byte defecto;
	
	private byte activa;

	@Column(length=8)
	private String numero;

	@Column(length=45)
	private String pais;

	@Column(length=8)
	private String piso;

	@Column(length=8)
	private String portal;

	@Column(length=8)
	private String provincia;

	@Column(length=8)
	private String puerta;

	@Column(length=10)
	private String telefono;

	@Column(length=6)
	private String zip;

	//bi-directional many-to-one association to Tipodireccion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tipoDireccion_idTipoDireccion", nullable=false)
	private Tipodireccion tipodireccion;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="users_user_id", nullable=false)
	private User user;

	//bi-directional many-to-one association to Pedido
	@OneToMany(mappedBy="direccion")
	private Set<Pedido> pedidos;

	public Direccion() {
	}

	public int getIdDireccion() {
		return this.idDireccion;
	}

	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}

	public String getNombreDireccion() {
		return nombreDireccion;
	}

	public void setNombreDireccion(String nombreDireccion) {
		this.nombreDireccion = nombreDireccion;
	}

	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public byte getDefecto() {
		return this.defecto;
	}

	public void setDefecto(byte defecto) {
		this.defecto = defecto;
	}
	
	public byte getActiva() {
		return activa;
	}

	public void setActiva(byte activa) {
		this.activa = activa;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getPiso() {
		return this.piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getPortal() {
		return this.portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public String getProvincia() {
		return this.provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getPuerta() {
		return this.puerta;
	}

	public void setPuerta(String puerta) {
		this.puerta = puerta;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Tipodireccion getTipodireccion() {
		return this.tipodireccion;
	}

	public void setTipodireccion(Tipodireccion tipodireccion) {
		this.tipodireccion = tipodireccion;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Pedido> getPedidos() {
		return this.pedidos;
	}

	public void setPedidos(Set<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Pedido addPedido(Pedido pedido) {
		getPedidos().add(pedido);
		pedido.setDireccion(this);

		return pedido;
	}

	public Pedido removePedido(Pedido pedido) {
		getPedidos().remove(pedido);
		pedido.setDireccion(null);

		return pedido;
	}
	@Override
	public int hashCode() {
	    return idDireccion;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Direccion && this.idDireccion == ((Direccion)object).idDireccion) {
        	isEqual = true;
        }
        return isEqual;
    }
	
	public String toString(){
		String result;
		
		result = nombreDireccion + " " + numero + " " + portal + " " + piso + " " + puerta + " " + zip + " " + ciudad + " " + provincia + " " + pais;
		
		return result;
	}
}