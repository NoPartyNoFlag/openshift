package es.webapp.exiquets.domain.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the usuarioanonimos_has_productos database table.
 * 
 */
@Entity
@Table(name="usuarioanonimos_has_productos")
@NamedQuery(name="UsuarioanonimosHasProducto.findAll", query="SELECT u FROM UsuarioanonimosHasProducto u")
public class UsuarioanonimosHasProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UsuarioanonimosHasProductoPK id;

	private Integer cantidad = 1;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="productos_idProductos", nullable=false, insertable=false, updatable=false)
	private Producto producto;

	//bi-directional many-to-one association to Usuariosanonimo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="usuariosAnonimos_idUsuarioAnonimo", nullable=false, insertable=false, updatable=false)
	private Usuariosanonimo usuariosanonimo;

	public UsuarioanonimosHasProducto() {
	}

	public UsuarioanonimosHasProductoPK getId() {
		return this.id;
	}

	public void setId(UsuarioanonimosHasProductoPK id) {
		this.id = id;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Usuariosanonimo getUsuariosanonimo() {
		return this.usuariosanonimo;
	}

	public void setUsuariosanonimo(Usuariosanonimo usuariosanonimo) {
		this.usuariosanonimo = usuariosanonimo;
	}
	
	public int hashCode() {
	    return Integer.parseInt(String.valueOf(id.getProductos_idProductos()) + String.valueOf(id.getUsuariosAnonimos_idUsuarioAnonimo()));
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof UsuarioanonimosHasProducto && this.id.equals(((UsuarioanonimosHasProducto)object).id)) {
        	isEqual = true;
        }

        return isEqual;
    }

}