package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the users_has_productos database table.
 * 
 */
@Embeddable
public class UsersHasProductoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="users_user_id", insertable=false, updatable=false, unique=true, nullable=false)
	private int usersUserId;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int productos_idProductos;

	public UsersHasProductoPK() {
	}
	public int getUsersUserId() {
		return this.usersUserId;
	}
	public void setUsersUserId(int usersUserId) {
		this.usersUserId = usersUserId;
	}
	public int getProductos_idProductos() {
		return this.productos_idProductos;
	}
	public void setProductos_idProductos(int productos_idProductos) {
		this.productos_idProductos = productos_idProductos;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UsersHasProductoPK)) {
			return false;
		}
		UsersHasProductoPK castOther = (UsersHasProductoPK)other;
		return 
			(this.usersUserId == castOther.usersUserId)
			&& (this.productos_idProductos == castOther.productos_idProductos);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.usersUserId;
		hash = hash * prime + this.productos_idProductos;
		
		return hash;
	}
}