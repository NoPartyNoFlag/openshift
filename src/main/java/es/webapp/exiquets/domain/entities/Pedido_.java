package es.webapp.exiquets.domain.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.852+0200")
@StaticMetamodel(Pedido.class)
public class Pedido_ {
	public static volatile SingularAttribute<Pedido, Integer> idPedido;
	public static volatile SingularAttribute<Pedido, Date> fechaPedido;
	public static volatile SingularAttribute<Pedido, Direccion> direccion;
	public static volatile SingularAttribute<Pedido, EstadoPedido> estadoPedido;
	public static volatile SingularAttribute<Pedido, User> user;
	public static volatile SetAttribute<Pedido, PedidoHasProducto> pedidoHasProductos;
	public static volatile SetAttribute<Pedido, Producto> productos;
}
