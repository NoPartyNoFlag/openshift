package es.webapp.exiquets.domain.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.977+0200")
@StaticMetamodel(Usuariosanonimo.class)
public class Usuariosanonimo_ {
	public static volatile SingularAttribute<Usuariosanonimo, Integer> idUsuarioAnonimo;
	public static volatile SingularAttribute<Usuariosanonimo, String> idSesion;
	public static volatile SetAttribute<Usuariosanonimo, UsuarioanonimosHasProducto> usuarioanonimosHasProductos;
	public static volatile SetAttribute<Usuariosanonimo, Producto> productos;
	public static volatile SingularAttribute<Usuariosanonimo, Date> fechaCreacion;
}
