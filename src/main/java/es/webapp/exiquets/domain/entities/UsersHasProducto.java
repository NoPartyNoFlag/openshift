package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the users_has_productos database table.
 * 
 */
@Entity
@Table(name="users_has_productos")
@NamedQuery(name="UsersHasProducto.findAll", query="SELECT u FROM UsersHasProducto u")
public class UsersHasProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UsersHasProductoPK id;

	private Integer cantidad = 0;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="Productos_idProductos", nullable=false, insertable=false, updatable=false)
	private Producto producto;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="users_user_id", nullable=false, insertable=false, updatable=false)
	private User user;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion = new Date();

	public UsersHasProducto() {
	}

	public UsersHasProductoPK getId() {
		return this.id;
	}

	public void setId(UsersHasProductoPK id) {
		this.id = id;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public int hashCode() {
	    return Integer.parseInt(String.valueOf(id.getProductos_idProductos()) + String.valueOf(id.getUsersUserId()));
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof UsersHasProducto && this.id.equals(((UsersHasProducto)object).id)) {
        	isEqual = true;
        }

        return isEqual;
    }

}