package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the usuarioanonimos_has_productos database table.
 * 
 */
@Embeddable
public class UsuarioanonimosHasProductoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int usuariosAnonimos_idUsuarioAnonimo;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int productos_idProductos;

	public UsuarioanonimosHasProductoPK() {
	}
	public int getUsuariosAnonimos_idUsuarioAnonimo() {
		return this.usuariosAnonimos_idUsuarioAnonimo;
	}
	public void setUsuariosAnonimos_idUsuarioAnonimo(int usuariosAnonimos_idUsuarioAnonimo) {
		this.usuariosAnonimos_idUsuarioAnonimo = usuariosAnonimos_idUsuarioAnonimo;
	}
	public int getProductos_idProductos() {
		return this.productos_idProductos;
	}
	public void setProductos_idProductos(int productos_idProductos) {
		this.productos_idProductos = productos_idProductos;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UsuarioanonimosHasProductoPK)) {
			return false;
		}
		UsuarioanonimosHasProductoPK castOther = (UsuarioanonimosHasProductoPK)other;
		return 
			(this.usuariosAnonimos_idUsuarioAnonimo == castOther.usuariosAnonimos_idUsuarioAnonimo)
			&& (this.productos_idProductos == castOther.productos_idProductos);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.usuariosAnonimos_idUsuarioAnonimo;
		hash = hash * prime + this.productos_idProductos;
		
		return hash;
	}
}