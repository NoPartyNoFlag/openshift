package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.930+0200")
@StaticMetamodel(UsersHasProductoPK.class)
public class UsersHasProductoPK_ {
	public static volatile SingularAttribute<UsersHasProductoPK, Integer> usersUserId;
	public static volatile SingularAttribute<UsersHasProductoPK, Integer> productos_idProductos;
}
