package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T19:22:24.308+0200")
@StaticMetamodel(Producto.class)
public class Producto_ {
	public static volatile SingularAttribute<Producto, Integer> idProductos;
	public static volatile SingularAttribute<Producto, String> img;
	public static volatile SingularAttribute<Producto, Float> precio;
	public static volatile SingularAttribute<Producto, Integer> stock;
	public static volatile SetAttribute<Producto, Descripcion> descripcions;
	public static volatile SetAttribute<Producto, Nombre> nombres;
	public static volatile SetAttribute<Producto, PedidoHasProducto> pedidoHasProductos;
	public static volatile SingularAttribute<Producto, Categoria> categoriaBean;
	public static volatile SetAttribute<Producto, Pedido> pedidos;
	public static volatile SetAttribute<Producto, User> users;
	public static volatile SetAttribute<Producto, UsersHasProducto> usersHasProductos;
	public static volatile SetAttribute<Producto, UsuarioanonimosHasProducto> usuarioanonimosHasProductos;
	public static volatile SetAttribute<Producto, Usuariosanonimo> usuariosanonimos;
}
