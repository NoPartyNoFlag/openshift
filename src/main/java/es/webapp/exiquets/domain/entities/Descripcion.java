package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the descripcion database table.
 * 
 */
@Entity
@Table(name="descripcion")
@NamedQuery(name="Descripcion.findAll", query="SELECT d FROM Descripcion d")
public class Descripcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idDescripcion;

	@Column(length=255)
	private String literal;

	//bi-directional many-to-one association to Idioma
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idIdioma", nullable=false)
	private Idioma idioma;

	//bi-directional many-to-many association to Producto
	@ManyToMany
	@JoinTable(
		name="productos_has_descripcion"
		, joinColumns={
			@JoinColumn(name="descripcion_idDescripcion", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="productos_idProductos", nullable=false)
			}
		)
	private Set<Producto> productos = new HashSet<Producto>();

	public Descripcion() {
	}

	public int getIdDescripcion() {
		return this.idDescripcion;
	}

	public void setIdDescripcion(int idDescripcion) {
		this.idDescripcion = idDescripcion;
	}

	public String getLiteral() {
		return this.literal;
	}

	public void setLiteral(String literal) {
		this.literal = literal;
	}

	public Idioma getIdioma() {
		return this.idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}

	public Set<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(Set<Producto> productos) {
		this.productos = productos;
	}
	@Override
	public int hashCode() {
	    return idDescripcion;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Descripcion && this.idDescripcion == ((Descripcion)object).idDescripcion) {
        	isEqual = true;
        }

        return isEqual;
    }
}