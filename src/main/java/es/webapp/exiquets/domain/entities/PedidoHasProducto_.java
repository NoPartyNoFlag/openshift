package es.webapp.exiquets.domain.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.867+0200")
@StaticMetamodel(PedidoHasProducto.class)
public class PedidoHasProducto_ {
	public static volatile SingularAttribute<PedidoHasProducto, PedidoHasProductoPK> id;
	public static volatile SingularAttribute<PedidoHasProducto, Integer> cantidad;
	public static volatile SingularAttribute<PedidoHasProducto, Pedido> pedido;
	public static volatile SingularAttribute<PedidoHasProducto, Producto> producto;
	public static volatile SingularAttribute<PedidoHasProducto, Date> fechaModificacion;
}
