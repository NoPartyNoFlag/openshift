package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the idiomas database table.
 * 
 */
@Entity
@Table(name="idiomas")
@NamedQuery(name="Idioma.findAll", query="SELECT i FROM Idioma i")
public class Idioma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idIdiomas;

	@Column(nullable=false, length=45)
	private String idioma;

	@Column(nullable=false, length=45)
	private String localeCode;
	
	@Column(nullable=false, length=45)
	private String idiomaCode;

	//bi-directional many-to-one association to Descripcion
	@OneToMany(mappedBy="idioma")
	private Set<Descripcion> descripcions;

	//bi-directional many-to-one association to Nombre
	@OneToMany(mappedBy="idioma")
	private Set<Nombre> nombres;

	//bi-directional many-to-one association to Nombrescategoria
	@OneToMany(mappedBy="idioma")
	private Set<Nombrescategoria> nombrescategorias;

	public Idioma() {
		idioma="";
		localeCode="";
		idiomaCode="";
	}

	public int getIdIdiomas() {
		return this.idIdiomas;
	}

	public void setIdIdiomas(int idIdiomas) {
		this.idIdiomas = idIdiomas;
	}

	public String getIdioma() {
		return this.idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getLocaleCode() {
		return this.localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}
	
	public String getIdiomaCode() {
		return idiomaCode;
	}

	public void setIdiomaCode(String idiomaCode) {
		this.idiomaCode = idiomaCode;
	}

	public Set<Descripcion> getDescripcions() {
		return this.descripcions;
	}

	public void setDescripcions(Set<Descripcion> descripcions) {
		this.descripcions = descripcions;
	}

	public Descripcion addDescripcion(Descripcion descripcion) {
		getDescripcions().add(descripcion);
		descripcion.setIdioma(this);

		return descripcion;
	}

	public Descripcion removeDescripcion(Descripcion descripcion) {
		getDescripcions().remove(descripcion);
		descripcion.setIdioma(null);

		return descripcion;
	}

	public Set<Nombre> getNombres() {
		return this.nombres;
	}

	public void setNombres(Set<Nombre> nombres) {
		this.nombres = nombres;
	}

	public Nombre addNombre(Nombre nombre) {
		getNombres().add(nombre);
		nombre.setIdioma(this);

		return nombre;
	}

	public Nombre removeNombre(Nombre nombre) {
		getNombres().remove(nombre);
		nombre.setIdioma(null);

		return nombre;
	}

	public Set<Nombrescategoria> getNombrescategorias() {
		return this.nombrescategorias;
	}

	public void setNombrescategorias(Set<Nombrescategoria> nombrescategorias) {
		this.nombrescategorias = nombrescategorias;
	}

	public Nombrescategoria addNombrescategoria(Nombrescategoria nombrescategoria) {
		getNombrescategorias().add(nombrescategoria);
		nombrescategoria.setIdioma(this);

		return nombrescategoria;
	}

	public Nombrescategoria removeNombrescategoria(Nombrescategoria nombrescategoria) {
		getNombrescategorias().remove(nombrescategoria);
		nombrescategoria.setIdioma(null);

		return nombrescategoria;
	}
	@Override
	public int hashCode() {
	    return idIdiomas;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Idioma && this.idIdiomas == ((Idioma)object).idIdiomas) {
        	isEqual = true;
        }

        return isEqual;
    }
}