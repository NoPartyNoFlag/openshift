package es.webapp.exiquets.domain.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Set;


/**
 * The persistent class for the tipodireccion database table.
 * 
 */
@Entity
@Table(name="tipoDireccion")
@NamedQuery(name="Tipodireccion.findAll", query="SELECT t FROM Tipodireccion t")
public class Tipodireccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idTipoDireccion;

	//bi-directional many-to-one association to Direccion
	@OneToMany(mappedBy="tipodireccion")
	private Set<Direccion> direccions;

	public Tipodireccion() {
	}

	public int getIdTipoDireccion() {
		return this.idTipoDireccion;
	}

	public void setIdTipoDireccion(int idTipoDireccion) {
		this.idTipoDireccion = idTipoDireccion;
	}

	public Set<Direccion> getDireccions() {
		return this.direccions;
	}

	public void setDireccions(Set<Direccion> direccions) {
		this.direccions = direccions;
	}

	public Direccion addDireccion(Direccion direccion) {
		getDireccions().add(direccion);
		direccion.setTipodireccion(this);

		return direccion;
	}

	public Direccion removeDireccion(Direccion direccion) {
		getDireccions().remove(direccion);
		direccion.setTipodireccion(null);

		return direccion;
	}
	
	@Override
	public int hashCode() {
	    return idTipoDireccion;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Tipodireccion && this.idTipoDireccion == ((Tipodireccion)object).idTipoDireccion) {
        	isEqual = true;
        }

        return isEqual;
    }

}