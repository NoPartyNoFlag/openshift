package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.805+0200")
@StaticMetamodel(EstadoPedido.class)
public class EstadoPedido_ {
	public static volatile SingularAttribute<EstadoPedido, Integer> idEstadoPedido;
	public static volatile SetAttribute<EstadoPedido, Pedido> pedidos;
}
