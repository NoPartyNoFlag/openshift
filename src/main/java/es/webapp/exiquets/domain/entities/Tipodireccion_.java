package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.899+0200")
@StaticMetamodel(Tipodireccion.class)
public class Tipodireccion_ {
	public static volatile SingularAttribute<Tipodireccion, Integer> idTipoDireccion;
	public static volatile SetAttribute<Tipodireccion, Direccion> direccions;
}
