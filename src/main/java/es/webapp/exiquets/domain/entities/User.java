package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id", unique=true, nullable=false)
	private int userId;

	@Column(nullable=false)
	private byte enabled;

	@Column(nullable=false, length=255)
	private String mail;

	@Column(nullable=false, length=255)
	private String nombre;

	@Column(nullable=true, length=255)
	private String password;

	//bi-directional many-to-one association to Pedido
	@OneToMany(mappedBy="user")
	private Set<Pedido> pedidos = new HashSet<Pedido>();

	//bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy="user")
	private Set<UserRole> userRoles = new HashSet<UserRole>();

	//bi-directional many-to-many association to Producto
	@ManyToMany
	@JoinTable(
		name="users_has_productos"
		, joinColumns={
			@JoinColumn(name="users_user_id", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="Productos_idProductos", nullable=false)
			}
		)
	private Set<Producto> productos = new HashSet<Producto>();

	//bi-directional many-to-one association to UsersHasProducto
	@OneToMany(mappedBy="user")
	private Set<UsersHasProducto> usersHasProductos = new HashSet<UsersHasProducto>();
	
	@OneToMany(mappedBy="user")
	private Set<Direccion> direccions;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion = new Date();

	public User() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public byte getEnabled() {
		return this.enabled;
	}

	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Pedido> getPedidos() {
		return this.pedidos;
	}

	public void setPedidos(Set<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Pedido addPedido(Pedido pedido) {
		getPedidos().add(pedido);
		pedido.setUser(this);

		return pedido;
	}

	public Pedido removePedido(Pedido pedido) {
		getPedidos().remove(pedido);
		pedido.setUser(null);

		return pedido;
	}

	public Set<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public UserRole addUserRole(UserRole userRole) {
		getUserRoles().add(userRole);
		userRole.setUser(this);

		return userRole;
	}

	public UserRole removeUserRole(UserRole userRole) {
		getUserRoles().remove(userRole);
		userRole.setUser(null);

		return userRole;
	}

	public Set<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(Set<Producto> productos) {
		this.productos = productos;
	}

	public Set<UsersHasProducto> getUsersHasProductos() {
		return this.usersHasProductos;
	}

	public void setUsersHasProductos(Set<UsersHasProducto> usersHasProductos) {
		this.usersHasProductos = usersHasProductos;
	}

	public UsersHasProducto addUsersHasProducto(UsersHasProducto usersHasProducto) {
		getUsersHasProductos().add(usersHasProducto);
		usersHasProducto.setUser(this);

		return usersHasProducto;
	}

	public UsersHasProducto removeUsersHasProducto(UsersHasProducto usersHasProducto) {
		getUsersHasProductos().remove(usersHasProducto);
		usersHasProducto.setUser(null);

		return usersHasProducto;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public Set<Direccion> getDireccions() {
		return this.direccions;
	}

	public void setDireccions(Set<Direccion> direccions) {
		this.direccions = direccions;
	}

	@Override
	public int hashCode() {
	    return userId;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof User && this.userId == ((User)object).userId) {
        	isEqual = true;
        }

        return isEqual;
	}
}