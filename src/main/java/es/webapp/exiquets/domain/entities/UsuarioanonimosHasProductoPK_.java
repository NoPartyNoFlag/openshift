package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.961+0200")
@StaticMetamodel(UsuarioanonimosHasProductoPK.class)
public class UsuarioanonimosHasProductoPK_ {
	public static volatile SingularAttribute<UsuarioanonimosHasProductoPK, Integer> usuariosAnonimos_idUsuarioAnonimo;
	public static volatile SingularAttribute<UsuarioanonimosHasProductoPK, Integer> productos_idProductos;
}
