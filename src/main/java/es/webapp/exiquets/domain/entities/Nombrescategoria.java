package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the nombrescategorias database table.
 * 
 */
@Entity
@Table(name="nombrescategorias")
@NamedQuery(name="Nombrescategoria.findAll", query="SELECT n FROM Nombrescategoria n")
public class Nombrescategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idNombresCategorias;

	@Column(length=255)
	private String literalCategoria;

	//bi-directional many-to-many association to Categoria
	@ManyToMany
	@JoinTable(
		name="categorias_has_nombrescategorias"
		, joinColumns={
			@JoinColumn(name="nombresCategorias_idNombresCategorias", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="categorias_idCategorias", nullable=false)
			}
		)
	private Set<Categoria> categorias = new HashSet<Categoria>();

	//bi-directional many-to-one association to Idioma
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idIdiomas", nullable=false)
	private Idioma idioma;

	public Nombrescategoria() {
	}

	public int getIdNombresCategorias() {
		return this.idNombresCategorias;
	}

	public void setIdNombresCategorias(int idNombresCategorias) {
		this.idNombresCategorias = idNombresCategorias;
	}

	public String getLiteralCategoria() {
		return this.literalCategoria;
	}

	public void setLiteralCategoria(String literalCategoria) {
		this.literalCategoria = literalCategoria;
	}

	public Set<Categoria> getCategorias() {
		return this.categorias;
	}

	public void setCategorias(Set<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Idioma getIdioma() {
		return this.idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}
	@Override
	public int hashCode() {
	    return idNombresCategorias;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Nombrescategoria && this.idNombresCategorias == ((Nombrescategoria)object).idNombresCategorias) {
        	isEqual = true;
        }

        return isEqual;
    }
}