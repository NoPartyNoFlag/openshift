package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the pedido_has_productos database table.
 * 
 */
@Entity
@Table(name="pedido_has_productos")
@NamedQuery(name="PedidoHasProducto.findAll", query="SELECT p FROM PedidoHasProducto p")
public class PedidoHasProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PedidoHasProductoPK id;
	
	transient private int idPedidoHasProducto;

	private int cantidad;

	//bi-directional many-to-one association to Pedido
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pedido_idPedido", nullable=false, insertable=false, updatable=false)
	private Pedido pedido;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="productos_idProductos", nullable=false, insertable=false, updatable=false)
	private Producto producto;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion = new Date();

	public PedidoHasProducto() {
	}

	public PedidoHasProductoPK getId() {
		return this.id;
	}

	public void setId(PedidoHasProductoPK id) {
		this.id = id;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Pedido getPedido() {
		return this.pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	public int hashCode() {
	    return Integer.parseInt(String.valueOf(id.getProductos_idProductos()) + String.valueOf(id.getPedido_idPedido()));
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof PedidoHasProducto && this.id.equals(((PedidoHasProducto)object).id)) {
        	isEqual = true;
        }

        return isEqual;
    }

}