package es.webapp.exiquets.domain.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.899+0200")
@StaticMetamodel(User.class)
public class User_ {
	public static volatile SingularAttribute<User, Integer> userId;
	public static volatile SingularAttribute<User, Byte> enabled;
	public static volatile SingularAttribute<User, String> mail;
	public static volatile SingularAttribute<User, String> nombre;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SetAttribute<User, Pedido> pedidos;
	public static volatile SetAttribute<User, UserRole> userRoles;
	public static volatile SetAttribute<User, Producto> productos;
	public static volatile SetAttribute<User, UsersHasProducto> usersHasProductos;
	public static volatile SetAttribute<User, Direccion> direccions;
	public static volatile SingularAttribute<User, Date> fechaCreacion;
}
