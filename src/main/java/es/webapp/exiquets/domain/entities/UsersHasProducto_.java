package es.webapp.exiquets.domain.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.914+0200")
@StaticMetamodel(UsersHasProducto.class)
public class UsersHasProducto_ {
	public static volatile SingularAttribute<UsersHasProducto, UsersHasProductoPK> id;
	public static volatile SingularAttribute<UsersHasProducto, Integer> cantidad;
	public static volatile SingularAttribute<UsersHasProducto, Producto> producto;
	public static volatile SingularAttribute<UsersHasProducto, User> user;
	public static volatile SingularAttribute<UsersHasProducto, Date> fechaModificacion;
}
