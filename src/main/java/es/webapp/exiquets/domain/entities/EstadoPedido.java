package es.webapp.exiquets.domain.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Set;


/**
 * The persistent class for the estado_pedido database table.
 * 
 */
@Entity
@Table(name="estado_pedido")
@NamedQuery(name="EstadoPedido.findAll", query="SELECT e FROM EstadoPedido e")
public class EstadoPedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idEstadoPedido;

	//bi-directional many-to-one association to Pedido
	@OneToMany(mappedBy="estadoPedido")
	private Set<Pedido> pedidos;

	public EstadoPedido() {
	}

	public int getIdEstadoPedido() {
		return this.idEstadoPedido;
	}

	public void setIdEstadoPedido(int idEstadoPedido) {
		this.idEstadoPedido = idEstadoPedido;
	}

	public Set<Pedido> getPedidos() {
		return this.pedidos;
	}

	public void setPedidos(Set<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Pedido addPedido(Pedido pedido) {
		getPedidos().add(pedido);
		pedido.setEstadoPedido(this);

		return pedido;
	}

	public Pedido removePedido(Pedido pedido) {
		getPedidos().remove(pedido);
		pedido.setEstadoPedido(null);

		return pedido;
	}

	@Override
	public int hashCode() {
	    return idEstadoPedido;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof EstadoPedido && this.idEstadoPedido == ((EstadoPedido)object).idEstadoPedido) {
        	isEqual = true;
        }

        return isEqual;
    }
}