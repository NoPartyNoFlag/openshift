package es.webapp.exiquets.domain.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the productos database table.
 * 
 */
@Entity
@Table(name="productos")
@NamedQuery(name="Producto.findAll", query="SELECT p FROM Producto p")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idProductos;

	private String img;

	private float precio;

	private int stock;

	//bi-directional many-to-many association to Descripcion
	@ManyToMany(mappedBy="productos")
	private Set<Descripcion> descripcions = new HashSet<Descripcion>();

	//bi-directional many-to-many association to Nombre
	@ManyToMany(mappedBy="productos")
	private Set<Nombre> nombres = new HashSet<Nombre>();

	//bi-directional many-to-one association to PedidoHasProducto
	@OneToMany(mappedBy="producto")
	private Set<PedidoHasProducto> pedidoHasProductos = new HashSet<PedidoHasProducto>();

	//bi-directional many-to-one association to Categoria
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="categoria")
	private Categoria categoriaBean;

	//bi-directional many-to-many association to Pedido
	@ManyToMany
	@JoinTable(
		name="pedido_has_productos"
		, joinColumns={
			@JoinColumn(name="productos_idProductos", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="pedido_idPedido", nullable=false)
			}
		)
	private Set<Pedido> pedidos = new HashSet<Pedido>();

	//bi-directional many-to-many association to User
	@ManyToMany(mappedBy="productos")
	private Set<User> users = new HashSet<User>();

	//bi-directional many-to-one association to UsersHasProducto
	@OneToMany(mappedBy="producto")
	private Set<UsersHasProducto> usersHasProductos = new HashSet<UsersHasProducto>();

	//bi-directional many-to-one association to UsuarioanonimosHasProducto
	@OneToMany(mappedBy="producto")
	private Set<UsuarioanonimosHasProducto> usuarioanonimosHasProductos = new HashSet<UsuarioanonimosHasProducto>();

	//bi-directional many-to-many association to Usuariosanonimo
	@ManyToMany(mappedBy="productos")
	private Set<Usuariosanonimo> usuariosanonimos = new HashSet<Usuariosanonimo>();

	public Producto() {
	}

	public int getIdProductos() {
		return this.idProductos;
	}

	public void setIdProductos(int idProductos) {
		this.idProductos = idProductos;
	}

	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public float getPrecio() {
		return this.precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getStock() {
		return this.stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Set<Descripcion> getDescripcions() {
		return this.descripcions;
	}

	public void setDescripcions(Set<Descripcion> descripcions) {
		this.descripcions = descripcions;
	}

	public Set<Nombre> getNombres() {
		return this.nombres;
	}

	public void setNombres(Set<Nombre> nombres) {
		this.nombres = nombres;
	}

	public Set<PedidoHasProducto> getPedidoHasProductos() {
		return this.pedidoHasProductos;
	}

	public void setPedidoHasProductos(Set<PedidoHasProducto> pedidoHasProductos) {
		this.pedidoHasProductos = pedidoHasProductos;
	}

	public PedidoHasProducto addPedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().add(pedidoHasProducto);
		pedidoHasProducto.setProducto(this);

		return pedidoHasProducto;
	}

	public PedidoHasProducto removePedidoHasProducto(PedidoHasProducto pedidoHasProducto) {
		getPedidoHasProductos().remove(pedidoHasProducto);
		pedidoHasProducto.setProducto(null);

		return pedidoHasProducto;
	}

	public Categoria getCategoriaBean() {
		return this.categoriaBean;
	}

	public void setCategoriaBean(Categoria categoriaBean) {
		this.categoriaBean = categoriaBean;
	}

	public Set<Pedido> getPedidos() {
		return this.pedidos;
	}

	public void setPedidos(Set<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<UsersHasProducto> getUsersHasProductos() {
		return this.usersHasProductos;
	}

	public void setUsersHasProductos(Set<UsersHasProducto> usersHasProductos) {
		this.usersHasProductos = usersHasProductos;
	}

	public UsersHasProducto addUsersHasProducto(UsersHasProducto usersHasProducto) {
		getUsersHasProductos().add(usersHasProducto);
		usersHasProducto.setProducto(this);

		return usersHasProducto;
	}

	public UsersHasProducto removeUsersHasProducto(UsersHasProducto usersHasProducto) {
		getUsersHasProductos().remove(usersHasProducto);
		usersHasProducto.setProducto(null);

		return usersHasProducto;
	}

	public Set<UsuarioanonimosHasProducto> getUsuarioanonimosHasProductos() {
		return this.usuarioanonimosHasProductos;
	}

	public void setUsuarioanonimosHasProductos(Set<UsuarioanonimosHasProducto> usuarioanonimosHasProductos) {
		this.usuarioanonimosHasProductos = usuarioanonimosHasProductos;
	}

	public UsuarioanonimosHasProducto addUsuarioanonimosHasProducto(UsuarioanonimosHasProducto usuarioanonimosHasProducto) {
		getUsuarioanonimosHasProductos().add(usuarioanonimosHasProducto);
		usuarioanonimosHasProducto.setProducto(this);

		return usuarioanonimosHasProducto;
	}

	public UsuarioanonimosHasProducto removeUsuarioanonimosHasProducto(UsuarioanonimosHasProducto usuarioanonimosHasProducto) {
		getUsuarioanonimosHasProductos().remove(usuarioanonimosHasProducto);
		usuarioanonimosHasProducto.setProducto(null);

		return usuarioanonimosHasProducto;
	}

	public Set<Usuariosanonimo> getUsuariosanonimos() {
		return this.usuariosanonimos;
	}

	public void setUsuariosanonimos(Set<Usuariosanonimo> usuariosanonimos) {
		this.usuariosanonimos = usuariosanonimos;
	}
	@Override
	public int hashCode() {
	    return idProductos;
	}
	
	@Override
    public boolean equals(Object object) {
		boolean isEqual = false;
        if (object instanceof Producto && this.idProductos == ((Producto)object).idProductos) {
        	isEqual = true;
        }

        return isEqual;
    }
	
	public String getLiteralNombre(String locale){
		String nombre = "No hay literal en " + locale;
		
		for(Nombre n :nombres){
			if(n.getIdioma().getIdiomaCode().equals(locale)){
				nombre = n.getLiteral();
			}
		}
		return nombre;
	}
	
	public String getLiteralDescripcion(String locale){
		String nombre = "No hay literal en " + locale;
		
		for(Descripcion d :descripcions){
			if(d.getIdioma().getIdiomaCode().equals(locale)){
				nombre = d.getLiteral();
			}
		}
		return nombre;
	}
}