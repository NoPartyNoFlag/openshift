package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.945+0200")
@StaticMetamodel(UsuarioanonimosHasProducto.class)
public class UsuarioanonimosHasProducto_ {
	public static volatile SingularAttribute<UsuarioanonimosHasProducto, UsuarioanonimosHasProductoPK> id;
	public static volatile SingularAttribute<UsuarioanonimosHasProducto, Integer> cantidad;
	public static volatile SingularAttribute<UsuarioanonimosHasProducto, Producto> producto;
	public static volatile SingularAttribute<UsuarioanonimosHasProducto, Usuariosanonimo> usuariosanonimo;
}
