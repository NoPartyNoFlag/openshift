package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.789+0200")
@StaticMetamodel(Direccion.class)
public class Direccion_ {
	public static volatile SingularAttribute<Direccion, Integer> idDireccion;
	public static volatile SingularAttribute<Direccion, String> nombreDireccion;
	public static volatile SingularAttribute<Direccion, String> ciudad;
	public static volatile SingularAttribute<Direccion, Byte> defecto;
	public static volatile SingularAttribute<Direccion, Byte> activa;
	public static volatile SingularAttribute<Direccion, String> numero;
	public static volatile SingularAttribute<Direccion, String> pais;
	public static volatile SingularAttribute<Direccion, String> piso;
	public static volatile SingularAttribute<Direccion, String> portal;
	public static volatile SingularAttribute<Direccion, String> provincia;
	public static volatile SingularAttribute<Direccion, String> puerta;
	public static volatile SingularAttribute<Direccion, String> telefono;
	public static volatile SingularAttribute<Direccion, String> zip;
	public static volatile SingularAttribute<Direccion, Tipodireccion> tipodireccion;
	public static volatile SingularAttribute<Direccion, User> user;
	public static volatile SetAttribute<Direccion, Pedido> pedidos;
}
