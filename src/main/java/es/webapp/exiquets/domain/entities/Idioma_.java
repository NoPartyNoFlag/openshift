package es.webapp.exiquets.domain.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-02T18:08:37.821+0200")
@StaticMetamodel(Idioma.class)
public class Idioma_ {
	public static volatile SingularAttribute<Idioma, Integer> idIdiomas;
	public static volatile SingularAttribute<Idioma, String> idioma;
	public static volatile SingularAttribute<Idioma, String> localeCode;
	public static volatile SingularAttribute<Idioma, String> idiomaCode;
	public static volatile SetAttribute<Idioma, Descripcion> descripcions;
	public static volatile SetAttribute<Idioma, Nombre> nombres;
	public static volatile SetAttribute<Idioma, Nombrescategoria> nombrescategorias;
}
