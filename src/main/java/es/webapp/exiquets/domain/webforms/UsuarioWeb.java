package es.webapp.exiquets.domain.webforms;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.Usuariosanonimo;

public class UsuarioWeb {
	private User usuarioRegistrado;
	private Usuariosanonimo usuarioAnonimo;
	private String tipoUsuario = "anonimo";
	public User getUsuarioRegistrado() {
		return usuarioRegistrado;
	}
	public void setUsuarioRegistrado(User usuarioRegistrado) {
		this.usuarioRegistrado = usuarioRegistrado;
	}
	public Usuariosanonimo getUsuarioAnonimo() {
		return usuarioAnonimo;
	}
	public void setUsuarioAnonimo(Usuariosanonimo usuarioAnonimo) {
		this.usuarioAnonimo = usuarioAnonimo;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	

}
