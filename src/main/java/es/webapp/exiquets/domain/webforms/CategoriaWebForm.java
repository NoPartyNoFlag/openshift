package es.webapp.exiquets.domain.webforms;

import java.util.Set;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.domain.entities.Producto;

public class CategoriaWebForm{
	int idCategoria;
	String nombreCategoria;
	boolean seleccionada;
	private Set<Producto> productos;
	
	public CategoriaWebForm(){
		this.idCategoria = 0;
		this.nombreCategoria = "";
		this.productos = null;
	}
	
	public boolean isSeleccionada() {
		return seleccionada;
	}

	public void setSeleccionada(boolean seleccionada) {
		this.seleccionada = seleccionada;
	}

	public CategoriaWebForm(Categoria c){
		this.idCategoria = c.getIdCategorias();
		this.productos = c.getProductos();
	}

	public int getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(int idCategora) {
		this.idCategoria = idCategora;
	}
	public String getNombreCategoria() {
		return nombreCategoria;
	}
	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}
	public Set<Producto> getProductos() {
		return productos;
	}
	public void setProductos(Set<Producto> productos) {
		this.productos = productos;
	}
}
