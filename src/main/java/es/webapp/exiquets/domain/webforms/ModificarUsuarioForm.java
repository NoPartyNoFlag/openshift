package es.webapp.exiquets.domain.webforms;

import es.webapp.exiquets.domain.entities.User;

public class ModificarUsuarioForm {

	private String nombre;
	private String mail;
	private String password;
	private String botonPulsado;
	
	
	public ModificarUsuarioForm(User u){
		this.nombre = u.getNombre();
		this.mail = u.getMail();
		this.password = u.getPassword();
		this.botonPulsado = "";
	}
	
	public ModificarUsuarioForm(){
		this.nombre = "";
		this.mail = "";
		this.password = "";
		this.botonPulsado = "";
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBotonPulsado() {
		return botonPulsado;
	}
	public void setBotonPulsado(String botonPulsado) {
		this.botonPulsado = botonPulsado;
	}
}
