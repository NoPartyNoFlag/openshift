package es.webapp.exiquets.domain.webforms;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.domain.entities.Producto;

public class ListaCategoriasWebForm {
	private List<CategoriaWebForm> categorias;
	
	public ListaCategoriasWebForm(){
		categorias = new ArrayList<CategoriaWebForm>();
	}
	
	public List<CategoriaWebForm> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<CategoriaWebForm> listaCategorias) {
		this.categorias = listaCategorias;
	}
	
	public void addCategoria(Categoria c){
		categorias.add(new CategoriaWebForm(c));
	}
	
	public void addAllCategoria(List<Categoria> listaCategorias){
		for(Categoria c : listaCategorias){
			addCategoria(c);
		}
	}

	
}
