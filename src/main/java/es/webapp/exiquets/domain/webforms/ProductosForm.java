package es.webapp.exiquets.domain.webforms;

import java.util.ArrayList;
import java.util.List;

public class ProductosForm {

	private List<ProductoForm> productos;
	
	public ProductosForm(){
		this.productos = new ArrayList<ProductoForm>();
	}
	
	public ProductosForm(List<ProductoForm> listaProductos){
		this.productos = listaProductos;
	}

	public List<ProductoForm> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductoForm> productos) {
		this.productos = productos;
	}
	
	
	
}
