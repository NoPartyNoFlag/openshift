package es.webapp.exiquets.domain.webforms;

import es.webapp.exiquets.domain.entities.Categoria;

public class ProductoForm {

	private int idProductos;
	private Float precioMayor;
	private Float precioMenor;
	private Integer stock;
	private String nombre;
	private String descripcion;
	private Categoria categoriaBean = new Categoria();
	private boolean seleccionado;


	public boolean isSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	public int getIdProductos() {
		return idProductos;
	}

	public void setIdProductos(int idProductos) {
		this.idProductos = idProductos;
	}
	
	public Float getPrecioMayor() {
		return precioMayor;
	}

	public void setPrecioMayor(Float precioMayor) {
		this.precioMayor = precioMayor;
	}

	public Float getPrecioMenor() {
		return precioMenor;
	}

	public void setPrecioMenor(Float precioMenor) {
		this.precioMenor = precioMenor;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Categoria getCategoriaBean() {
		return categoriaBean;
	}

	public void setCategoriaBean(Categoria categoriaBean) {
		this.categoriaBean = categoriaBean;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
