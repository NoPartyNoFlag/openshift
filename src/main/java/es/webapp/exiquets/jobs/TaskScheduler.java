package es.webapp.exiquets.jobs;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.springframework.scheduling.annotation.Scheduled;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.UsersHasProducto;
import es.webapp.exiquets.domain.entities.Usuariosanonimo;
import es.webapp.exiquets.service.UsersHasProductoService;
import es.webapp.exiquets.service.UsuariosAnonimosService;
import es.webapp.exiquets.service.UsuariosService;

@Named
public class TaskScheduler {

	@Inject UsuariosAnonimosService gestorUsuariosAnonimos;
	@Inject UsuariosService gestorUsuariosRegistrados;
	@Inject UsersHasProductoService gestorUsersHasProductos;
//	Horas de duración de un usuario anónimo
	private int USUARIOS_ANONIMOS_TIMEOUT = 24; 
//	Horas de duracion de un productos en un carrito de un usuario registrado
	private int REGISTERED_USER_TIMEOUT_PRODUCT = 24;
	
	@Transactional
	@Scheduled(cron="0 0 2/12 * * ?")
	public void eliminaUsuariosExpirados(){
		List<Usuariosanonimo> listaUsuarios = gestorUsuariosAnonimos.findAll();
		
		for(Usuariosanonimo user : listaUsuarios){
			if(user.getFechaCreacion() != null){
				Calendar cal = Calendar.getInstance();
			    cal.setTime(user.getFechaCreacion());
			    cal.add(Calendar.HOUR_OF_DAY, USUARIOS_ANONIMOS_TIMEOUT);
			    Date timeoutDate = cal.getTime();
				
				if(timeoutDate.compareTo(new Date()) <= 0){
					gestorUsuariosAnonimos.deleteEntity(user);
				}
			}else{
				gestorUsuariosAnonimos.deleteEntity(user);
			}
		}
	}
	
	@Transactional
	@Scheduled(cron="0 0 4/12 * * ?")
	public void eliminaCarritosExpirados(){
		List<User> listaUsuarios = gestorUsuariosRegistrados.findAll();
		
		for(User user : listaUsuarios){
			for(UsersHasProducto producto : user.getUsersHasProductos()){
				if(producto.getFechaModificacion() != null){
					Calendar cal = Calendar.getInstance();
				    cal.setTime(producto.getFechaModificacion());
				    cal.add(Calendar.HOUR_OF_DAY, REGISTERED_USER_TIMEOUT_PRODUCT);
				    Date timeoutDate = cal.getTime();
				    
				    if(timeoutDate.compareTo(new Date()) <= 0){
				    	gestorUsersHasProductos.deleteEntity(producto);
				    }
				}else{
					gestorUsersHasProductos.deleteEntity(producto);
				}
			}
		}
	}
	
	
	
}
