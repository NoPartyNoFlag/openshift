package es.webapp.exiquets.service;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Usuariosanonimo;
import es.webapp.exiquets.repository.UsuariosAnonimosDAO;

@Component
public class UsuariosAnonimosServiceImpl extends BaseServiceExiquetsImpl<Usuariosanonimo,UsuariosAnonimosDAO> implements UsuariosAnonimosService{

	public Usuariosanonimo getUsuarioPorSession(String sesion) {
		Usuariosanonimo user = (Usuariosanonimo) getDAO().getUsuarioPorSesion(sesion);
		if(user == null){
			user = new Usuariosanonimo();
			user.setIdSesion(sesion);
			getDAO().save(user);
			user = (Usuariosanonimo) getDAO().getUsuarioPorSesion(sesion);
		}
		return user ;
	}

}
