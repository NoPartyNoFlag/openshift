package es.webapp.exiquets.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.repository.ProductosDAO;
import es.webapp.exiquets.repository.UsersHasProductosDAO;
import es.webapp.exiquets.repository.UsuarioAnonimosHasProductosDAO;

@Component
public class ProductosServiceImpl extends BaseServiceExiquetsImpl<Producto,ProductosDAO> implements ProductosService{
	
    @Autowired UsersHasProductosDAO usersHasProductosDAO;
    @Autowired UsuarioAnonimosHasProductosDAO usuarioAnonimosHasProductosDAO;
    
    static Logger logger = LoggerFactory.getLogger(ProductosServiceImpl.class);
    private static final String IMAGES_DIR = "images";
    private static final String DATA_SERVLET_CONTEXT = "dataDir";

	@Override
	public List<Producto> getProductosTodosIdiomas() {
		return getDAO().getProductosTodosIdiomas();
	}
	
	@Override
	public Producto getProductoTodosIdiomas(int id) {
		return getDAO().getProductoTodosIdiomas(id);
	}
	

	@Override
	public List<Producto> getProductos(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
		return getDAO().getAll(first, pageSize, sortField, sortOrder, filters);
	}
	

	@Override
	public int countProductos(Map<String,Object> filters) {
		return getDAO().count(filters);
	}

	@Override
	public synchronized boolean checkAndUpdateStock(Producto p, int cantidadSolicitada) {
		boolean result = true;
		if(p.getStock() < cantidadSolicitada){
			result = false;
		}else{
			p.setStock(p.getStock() - cantidadSolicitada);
			getDAO().update(p);
		}
		return result;
	}

	@Override
    public boolean guardarImgenEnDisco(UploadedFile image){
    	boolean result = true;
    	
    	String imagesDir = getImagesDir() + image.getFileName();
    	FileOutputStream fos = null;
    	
    	try {
			fos = new FileOutputStream(imagesDir);
			byte[] bytes = IOUtils.toByteArray(image.getInputstream());
			fos.write(bytes);
			logger.info("Imagen " + imagesDir + " guardada");
		} catch (IOException e) {
			result = false;
			logger.error(e.getMessage(),e);
		}finally{
			if(fos != null){
				try {
					fos.close();
				} catch (IOException e) {
					logger.error(e.getMessage(),e);
				}
			}
		}
    	
    	return result;
    }
	
	private String getImagesDir(){
    	String dataDir = System.getenv("OPENSHIFT_DATA_DIR");
    	if(dataDir == null){
    		dataDir = System.getProperty("OPENSHIFT_DATA_DIR");
    	}
    	String imagesDir = dataDir + File.separator + IMAGES_DIR + File.separator;
    	return imagesDir;
	}

	@Override
	public String getImagenDeDisco(String imageName) {
		return "/" + DATA_SERVLET_CONTEXT + "/" + IMAGES_DIR + "/" +  imageName;
	}

	@Override
	public boolean eliminarImagenDeDisco(String imageName) {
		File imageFile = new File(getImagesDir() + imageName);
		if(imageFile.exists()){
			imageFile.delete();
			logger.info("Imagen " + imageFile.getAbsolutePath() + " eliminada");
		}
		return false;
	}

}
