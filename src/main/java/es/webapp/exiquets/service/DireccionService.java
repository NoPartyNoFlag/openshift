package es.webapp.exiquets.service;

import java.util.List;

import es.webapp.exiquets.domain.entities.Direccion;
import es.webapp.exiquets.repository.DireccionDAO;

public interface DireccionService extends BaseServiceExiquets<Direccion, DireccionDAO> {
	public List<Direccion> getDireccionesFiltradas(Direccion filtro);
}
