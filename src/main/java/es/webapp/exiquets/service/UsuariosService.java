package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.repository.UsuariosDAO;

public interface UsuariosService extends BaseServiceExiquets<User, UsuariosDAO> {
	public User getUsuarioFiltrado(User u);
}
