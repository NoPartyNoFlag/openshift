package es.webapp.exiquets.service;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;

import es.webapp.exiquets.repository.BaseDAOeXiquets;

public abstract class BaseServiceExiquetsImpl<E extends Serializable,D extends BaseDAOeXiquets<E>> implements  BaseServiceExiquets<E,D> {
	
	@Autowired private D dao;
	
	@Override
	public void saveEntity(E entity) {
		dao.save(entity);
	}

	@Override
	public List<E> findAll() {
		return dao.findAll();
	}

	@Override
	public E getById(int id) {
		return dao.findOne(id);
	}

	@Override
	public void updateEntity(E entity) {
		dao.update(entity);
	}

	@Override
	public void deleteEntity(E entity) {
		dao.delete(entity);
	}

	@Override
	public EntityManager getEM() {
		return dao.getCurrentSession();
	}

	@Override
	public void setDAO(D dao) {
		this.dao = dao;
	}

	@Override
	public D getDAO() {
		return this.dao;
	}

}
