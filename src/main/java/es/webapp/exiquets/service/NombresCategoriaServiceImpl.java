package es.webapp.exiquets.service;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Nombrescategoria;
import es.webapp.exiquets.repository.NombresCategoriaDAO;

@Component
public class NombresCategoriaServiceImpl extends BaseServiceExiquetsImpl<Nombrescategoria,NombresCategoriaDAO> implements NombresCategoriaService{


}
