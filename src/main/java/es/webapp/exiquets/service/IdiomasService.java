package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.repository.IdiomasDAO;

public interface IdiomasService extends BaseServiceExiquets<Idioma, IdiomasDAO> {
	
}
