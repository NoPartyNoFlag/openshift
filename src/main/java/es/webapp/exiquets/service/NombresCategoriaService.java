package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.Nombrescategoria;
import es.webapp.exiquets.repository.NombresCategoriaDAO;

public interface NombresCategoriaService extends BaseServiceExiquets<Nombrescategoria, NombresCategoriaDAO> {
	
}
