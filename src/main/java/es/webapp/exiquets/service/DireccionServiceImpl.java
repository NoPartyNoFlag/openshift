package es.webapp.exiquets.service;

import java.util.List;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Direccion;
import es.webapp.exiquets.repository.DireccionDAO;

@Component
public class DireccionServiceImpl extends BaseServiceExiquetsImpl<Direccion,DireccionDAO> implements DireccionService{

	@Override
	public List<Direccion> getDireccionesFiltradas(Direccion filtro) {
		return getDAO().getDireccionesFiltradas(filtro);
	}


}
