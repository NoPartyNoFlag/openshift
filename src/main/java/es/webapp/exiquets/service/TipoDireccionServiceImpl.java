package es.webapp.exiquets.service;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Tipodireccion;
import es.webapp.exiquets.repository.TipoDireccionDAO;

@Component
public class TipoDireccionServiceImpl extends BaseServiceExiquetsImpl<Tipodireccion,TipoDireccionDAO> implements TipoDireccionService{


}
