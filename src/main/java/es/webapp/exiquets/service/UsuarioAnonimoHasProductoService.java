package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProducto;
import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProductoPK;
import es.webapp.exiquets.repository.UsuarioAnonimosHasProductosDAO;

public interface UsuarioAnonimoHasProductoService extends BaseServiceExiquets<UsuarioanonimosHasProducto, UsuarioAnonimosHasProductosDAO> {
	public UsuarioanonimosHasProducto getById(UsuarioanonimosHasProductoPK id);
	
}
