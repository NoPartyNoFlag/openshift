package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.EstadoPedido;
import es.webapp.exiquets.repository.EstadosPedidoDAO;

public interface EstadosPedidoService extends BaseServiceExiquets<EstadoPedido, EstadosPedidoDAO> {
	
}
