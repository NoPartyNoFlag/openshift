package es.webapp.exiquets.service;

import java.util.List;

import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.repository.RolUsuarioDAO;

public interface RolesService extends BaseServiceExiquets<UserRole, RolUsuarioDAO> {

	public List<UserRole> getGroupedRoles();
}
