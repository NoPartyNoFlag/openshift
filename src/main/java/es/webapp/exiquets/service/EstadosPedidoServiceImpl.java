package es.webapp.exiquets.service;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.EstadoPedido;
import es.webapp.exiquets.repository.EstadosPedidoDAO;

@Component
public class EstadosPedidoServiceImpl extends BaseServiceExiquetsImpl<EstadoPedido,EstadosPedidoDAO> implements EstadosPedidoService{


}
