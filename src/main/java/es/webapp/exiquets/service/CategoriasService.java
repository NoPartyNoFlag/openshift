package es.webapp.exiquets.service;

import java.util.List;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.repository.CategoriaDAO;

public interface CategoriasService extends BaseServiceExiquets<Categoria, CategoriaDAO> {
	
	public Categoria getCategoriaIdiomaSeleccionado(int id);
	public Categoria getCategoriaTodosIdiomas(int id);
	public List<Categoria> getCategoriasTodosIdiomas();
}
