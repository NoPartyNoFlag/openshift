package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.Descripcion;
import es.webapp.exiquets.repository.DescripcionesDAO;

public interface DescripcionesService extends BaseServiceExiquets<Descripcion, DescripcionesDAO> {
	
}
