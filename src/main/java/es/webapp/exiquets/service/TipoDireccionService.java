package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.Tipodireccion;
import es.webapp.exiquets.repository.TipoDireccionDAO;

public interface TipoDireccionService extends BaseServiceExiquets<Tipodireccion, TipoDireccionDAO> {
	
}
