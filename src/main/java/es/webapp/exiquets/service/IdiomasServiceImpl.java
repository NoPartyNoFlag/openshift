package es.webapp.exiquets.service;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.repository.IdiomasDAO;

@Component
public class IdiomasServiceImpl extends BaseServiceExiquetsImpl<Idioma,IdiomasDAO> implements IdiomasService{


}
