package es.webapp.exiquets.service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProducto;
import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProductoPK;
import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProductoPK_;
import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProducto_;
import es.webapp.exiquets.repository.UsuarioAnonimosHasProductosDAO;

@Component
public class UsuarioAnonimoHasProductoServiceImpl extends BaseServiceExiquetsImpl<UsuarioanonimosHasProducto,UsuarioAnonimosHasProductosDAO> implements UsuarioAnonimoHasProductoService{

	@Override
	public UsuarioanonimosHasProducto getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioanonimosHasProducto getById(UsuarioanonimosHasProductoPK id) {
		
		CriteriaBuilder builder = 	getDAO().getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<UsuarioanonimosHasProducto> criteriaQuery = builder.createQuery(UsuarioanonimosHasProducto.class);
		
		Root<UsuarioanonimosHasProducto> root = criteriaQuery.from(UsuarioanonimosHasProducto.class);
		
		Predicate predicate = builder.conjunction();
		predicate = builder.and(builder.equal(root.get(UsuarioanonimosHasProducto_.id).get(UsuarioanonimosHasProductoPK_.productos_idProductos), id.getProductos_idProductos()));
		predicate = builder.and(predicate,builder.equal(root.get(UsuarioanonimosHasProducto_.id).get(UsuarioanonimosHasProductoPK_.usuariosAnonimos_idUsuarioAnonimo), id.getUsuariosAnonimos_idUsuarioAnonimo()));
		
		criteriaQuery.where(predicate);
		
		return getDAO().getCurrentSession().createQuery(criteriaQuery).getSingleResult();
	}

}
