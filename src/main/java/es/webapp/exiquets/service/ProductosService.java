package es.webapp.exiquets.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;

import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.repository.ProductosDAO;

public interface ProductosService extends BaseServiceExiquets<Producto, ProductosDAO> {
    public List<Producto> getProductosTodosIdiomas();
    public Producto getProductoTodosIdiomas(int id);
    
    public List<Producto> getProductos(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters);
    public int countProductos(Map<String,Object> filters);
    
    public boolean checkAndUpdateStock(Producto p, int cantidadSolicitada);
    
    public boolean guardarImgenEnDisco(UploadedFile image);
    public String getImagenDeDisco(String imageName);
    public boolean eliminarImagenDeDisco(String imageName);
	
}
