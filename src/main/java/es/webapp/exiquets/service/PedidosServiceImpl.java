package es.webapp.exiquets.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Pedido;
import es.webapp.exiquets.domain.entities.PedidoHasProducto;
import es.webapp.exiquets.repository.PedidosDAO;
import es.webapp.exiquets.repository.PedidosHasProductosDAO;

@Component
public class PedidosServiceImpl extends BaseServiceExiquetsImpl<Pedido,PedidosDAO> implements PedidosService{
	@Autowired PedidosHasProductosDAO pedidosHasProductosDAO;

	@Override
	public void savePedidosHasProductos(PedidoHasProducto pedidoHasProducto) {
		pedidosHasProductosDAO.save(pedidoHasProducto);
	}

	@Override
	public void updatePedidosHasProduto(PedidoHasProducto pedidoHasProducto) {
		pedidosHasProductosDAO.update(pedidoHasProducto);
	}

	@Override
	public void deletePedidosHasProductos(PedidoHasProducto pedidoHasProducto) {
		pedidosHasProductosDAO.delete(pedidoHasProducto);		
	}

	@Override
	public int countPedidos(Map<String, Object> filters) {
		return getDAO().count(filters);
	}

	@Override
	public List<Pedido> getPedidos(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		return getDAO().getAll(first,pageSize,sortField,sortOrder,filters);
	}

	@Override
	public List<PedidoHasProducto> getPedidoHasProductoCompletoById(int id) {
		return pedidosHasProductosDAO.getPedidoHasProductoCompletoById(id);
	}

}
