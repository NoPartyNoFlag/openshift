package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.Nombre;
import es.webapp.exiquets.repository.NombresDAO;

public interface NombresService extends BaseServiceExiquets<Nombre, NombresDAO> {
	
}
