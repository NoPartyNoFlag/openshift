package es.webapp.exiquets.service;

import java.util.Collection;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.repository.UsuariosDAO;

@Component(value="usuariosService")
public class UsuariosServiceImpl extends BaseServiceExiquetsImpl<User,UsuariosDAO> implements UsuariosService, UserDetailsService{
	@Inject RolesService gestorRoles;
	

	@Override
	public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
		User userFilter = new User();
		userFilter.setMail(mail);
		User user = getDAO().getUsuarioFiltrado(userFilter);
		if (user == null) {
	        throw new UsernameNotFoundException("Invalid username/password.");
	    }
		String[] listaRolesArray = new String[user.getUserRoles().size()];
		int i = 0;
		for(UserRole role : user.getUserRoles()){
			listaRolesArray[i] = role.getAuthority();
			i++;
		}
		Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(listaRolesArray);
	    return new org.springframework.security.core.userdetails.User(user.getMail(), user.getPassword(), authorities);
	}

	@Override
	public User getUsuarioFiltrado(User u) {
		return getDAO().getUsuarioFiltrado(u);
	}

}
