package es.webapp.exiquets.service;

import java.util.List;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.UsersHasProducto;
import es.webapp.exiquets.repository.UsersHasProductosDAO;

@Component
public class UsersHasProductoServiceImpl extends BaseServiceExiquetsImpl<UsersHasProducto,UsersHasProductosDAO> implements UsersHasProductoService{

	@Override
	public UsersHasProducto getUserHasProductoCompleto(UsersHasProducto filtro) {
		List<UsersHasProducto> lista = getUsersHasProductoCompleto(filtro);
		return !lista.isEmpty()?lista.get(0):null;
	}

	@Override
	public List<UsersHasProducto> getUsersHasProductoCompleto(UsersHasProducto filtro) {
		return getDAO().getUsersHasProductoCompleto(filtro);
	}


}
