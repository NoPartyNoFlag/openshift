package es.webapp.exiquets.service;

import java.util.List;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.repository.CategoriaDAO;

@Component
public class CategoriasServiceImpl extends BaseServiceExiquetsImpl<Categoria,CategoriaDAO> implements CategoriasService{

	@Override
	public Categoria getCategoriaIdiomaSeleccionado(int id) {
		return getDAO().getCategoriaIdiomaSeleccionado(id);
	}

	@Override
	public Categoria getCategoriaTodosIdiomas(int id) {
		return getDAO().getCategoriaTodosIdiomas(id);
	}

	@Override
	public List<Categoria> getCategoriasTodosIdiomas() {
		return getDAO().getCategoriasTodosIdiomas();
	}


}
