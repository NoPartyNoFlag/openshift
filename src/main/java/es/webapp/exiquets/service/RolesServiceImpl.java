package es.webapp.exiquets.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.domain.entities.UserRole_;
import es.webapp.exiquets.domain.entities.User_;
import es.webapp.exiquets.repository.RolUsuarioDAO;

@Component
public class RolesServiceImpl extends BaseServiceExiquetsImpl<UserRole, RolUsuarioDAO> implements RolesService{
	
	
	@Override
	public List<UserRole> getGroupedRoles() {
		List<UserRole> listaRoles = new ArrayList<UserRole>();
		CriteriaBuilder criteriaBuilder = getDAO().getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<UserRole> criteriaQuery = criteriaBuilder.createQuery(UserRole.class);
		
		Root<UserRole> userRoleRoot = criteriaQuery.from(UserRole.class);
		
		criteriaQuery.groupBy(userRoleRoot.get(UserRole_.authority));
		
		CriteriaQuery<UserRole> cq = criteriaQuery.select(userRoleRoot);
		
		TypedQuery<UserRole> query = getDAO().getCurrentSession().createQuery(cq);
		listaRoles = query.getResultList();
		
		return listaRoles;
	}

}
