package es.webapp.exiquets.service;

import java.util.List;

import es.webapp.exiquets.domain.entities.UsersHasProducto;
import es.webapp.exiquets.repository.UsersHasProductosDAO;

public interface UsersHasProductoService extends BaseServiceExiquets<UsersHasProducto,UsersHasProductosDAO> {

	public UsersHasProducto getUserHasProductoCompleto(UsersHasProducto filtro);
	public List<UsersHasProducto> getUsersHasProductoCompleto(UsersHasProducto filtro);
	
}
