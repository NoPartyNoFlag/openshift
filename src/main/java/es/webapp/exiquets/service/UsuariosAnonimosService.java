package es.webapp.exiquets.service;

import es.webapp.exiquets.domain.entities.Usuariosanonimo;
import es.webapp.exiquets.repository.UsuariosAnonimosDAO;

public interface UsuariosAnonimosService extends BaseServiceExiquets<Usuariosanonimo, UsuariosAnonimosDAO> {
	public Usuariosanonimo getUsuarioPorSession(String sesion);	
}
