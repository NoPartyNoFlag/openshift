package es.webapp.exiquets.service;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Descripcion;
import es.webapp.exiquets.repository.DescripcionesDAO;

@Component
public class DescripcionesServiceImpl extends BaseServiceExiquetsImpl<Descripcion,DescripcionesDAO> implements DescripcionesService{


}
