package es.webapp.exiquets.service;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import es.webapp.exiquets.repository.BaseDAOeXiquets;

public interface BaseServiceExiquets<E extends Serializable,D extends BaseDAOeXiquets<E>> {

	public void setDAO(D dao);
	
	public D getDAO();
	
	public void saveEntity(E entity);
	
	public List<E> findAll();
	
	public E getById(int id);
	
	public void updateEntity(E entity);
	
	public void deleteEntity(E entity);
	
	public EntityManager getEM();
}
