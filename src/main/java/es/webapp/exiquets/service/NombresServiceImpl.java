package es.webapp.exiquets.service;

import org.springframework.stereotype.Component;

import es.webapp.exiquets.domain.entities.Nombre;
import es.webapp.exiquets.repository.NombresDAO;

@Component
public class NombresServiceImpl extends BaseServiceExiquetsImpl<Nombre,NombresDAO> implements NombresService{


}
