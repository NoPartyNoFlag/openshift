package es.webapp.exiquets.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import es.webapp.exiquets.domain.entities.Pedido;
import es.webapp.exiquets.domain.entities.PedidoHasProducto;
import es.webapp.exiquets.repository.PedidosDAO;

public interface PedidosService extends BaseServiceExiquets<Pedido, PedidosDAO> {
    public void savePedidosHasProductos(PedidoHasProducto pedidoHasProducto);
    
    public void updatePedidosHasProduto(PedidoHasProducto pedidoHasProducto);
    
    public void deletePedidosHasProductos(PedidoHasProducto pedidoHasProducto);


    public int countPedidos(Map<String,Object> filters);
    public List<Pedido> getPedidos(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters);
    
    public List<PedidoHasProducto> getPedidoHasProductoCompletoById(int idPedido);
}
