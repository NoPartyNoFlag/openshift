package es.webapp.exiquets.utils;

public class eXiquetsConstants {
	public static final String REGISTRO_USUARIO_NUEVO_SUCCES = "SAVE_NEW_USER_OK";
	public static final String DELETE_USUARIO_REGISTRADO = "DELETE_USUARIO";
	public static final String USER_SESION_KEY = "USER_SESSION_KEY";
	public static final String USUARIO_TIPO_ANONIMO = "USUARIO_TIPO_ANONIMO";
	public static final String USUARIO_TPO_REGISTRADO = "USUARIO_TIPO_REGISTRADO";
	
	
	public static final String ESTADO_PEDIDO_REALIZADO = "PEDIDO_REALIZADO";
	public static final String ESTADO_PEDIDO_ENVIADO = "PEDIDO_ENVIADO";
	public static final String ESTADO_PEDIDO_PROCESADO = "PEDIDO_PROCESADO";
}
