package es.webapp.exiquets.beans;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.SocialAuthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.service.RolesService;
import es.webapp.exiquets.service.UsuariosService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SocialAuthBean{
	
	@Inject private UsuariosService gestorUsuarios;
	@Inject private RolesService gestorRoles;
	@Inject private UserBean userBean;

	private SocialAuthManager manager;
    private String            originalURL;
    private String            providerID;
    private Profile           profile;
    
    static Logger logger = LoggerFactory.getLogger(SocialAuthBean.class);
    
    @PostConstruct
    public void setup(){
        // Initiate required components
        SocialAuthConfig config = SocialAuthConfig.getDefault();
        //load configuration. By default load the configuration from oauth_consumer.properties. 
        //You can also pass input stream, properties object or properties file name.
//      config.load(props);
        try {
        	String env = System.getenv("ENV");
        	if(env == null){
        		env = System.getProperty("ENV");
        	}
        	logger.debug("ENTORNO LEIDO:" + env);
			config.load(new ClassPathResource("/" + env + "/oauth_consumer.properties").getInputStream());
			manager = new SocialAuthManager();
	        manager.setSocialAuthConfig(config);
		} catch (Exception e) {
        	logger.error("ERROR MESSAGE: " + e.getMessage());
        	logger.error("ERROR LOCALIZED MESSAGE: " + e.getLocalizedMessage());
        	logger.error("ERROR STACKTRACE MESSAGE: " + e.getStackTrace().toString());
		}
        
    }
    
    public void socialConnect(){
        // 'successURL' is the page you'll be redirected to on successful login
//        ExternalContext externalContext   = FacesContext.getCurrentInstance().getExternalContext();
        String          successURL        = getRedirectURL(); 
        String authenticationURL;
		try {
			authenticationURL = manager.getAuthenticationUrl(providerID, successURL);
			FacesContext.getCurrentInstance().getExternalContext().redirect(authenticationURL);
		} catch (Exception e) {
        	logger.error("ERROR MESSAGE: " + e.getMessage());
        	logger.error("ERROR LOCALIZED MESSAGE: " + e.getLocalizedMessage());
        	logger.error("ERROR STACKTRACE MESSAGE: " + e.getStackTrace().toString());
		}
    }
    
    public void pullUserInfo() {
        try {
            // Pull user's data from the provider
            ExternalContext    externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request         = (HttpServletRequest) externalContext.getRequest();
            Map                map             = SocialAuthUtil.getRequestParametersMap(request);
            if (this.manager != null) {
                AuthProvider provider = manager.connect(map);
                this.profile          = provider.getUserProfile();
            
                // Do what you want with the data (e.g. persist to the database, etc.)
                logger.info("User's Social profile: " + profile);
                User userFilter = new User();
                userFilter.setMail(this.profile.getEmail());
                User usuarioRegistrado = null;
                try{
                	usuarioRegistrado = gestorUsuarios.getUsuarioFiltrado(userFilter);
                }catch(NoResultException e){
                	logger.error("Usuario no encontrado. Lo registramos");
                	usuarioRegistrado = new User();
                	usuarioRegistrado.setNombre(this.profile.getFullName());
                    usuarioRegistrado.setEnabled(new Byte("1"));
                    usuarioRegistrado.setPassword("NULL");
                    usuarioRegistrado.setFechaCreacion(new Date());
                    usuarioRegistrado.setMail(this.profile.getEmail());
             		gestorUsuarios.saveEntity(usuarioRegistrado);
             		UserRole userRole = new UserRole();
             		userRole.setUser(usuarioRegistrado);
             		userRole.setAuthority("ROLE_USER");
             		gestorRoles.saveEntity(userRole);
             		gestorRoles.getDAO().getCurrentSession().refresh(usuarioRegistrado);
                }

                
//                Loguear usuario en la app
                userBean.setUsuarioRegistrado(usuarioRegistrado);
                userBean.login();
            
                // Redirect the user back to where they have been before logging in
                FacesContext.getCurrentInstance().getExternalContext().redirect(originalURL);
            
            } else FacesContext.getCurrentInstance().getExternalContext().redirect(externalContext.getRequestContextPath() + "home.xhtml");
        } catch (Exception e) {
        	logger.error("ERROR MESSAGE: " + e.getMessage());
        	logger.error("ERROR LOCALIZED MESSAGE: " + e.getLocalizedMessage());
        	logger.error("ERROR CAUSE MESSAGE: " + e.getCause().getMessage());
        	logger.error("ERROR CAUSE LOCALIZEDMESSAGE: " + e.getCause().getLocalizedMessage());
        	logger.error("ERROR STACKTRACE MESSAGE: " + e.getStackTrace().toString());
        } 
    }
    
    public void logOut() {
        try {
            // Disconnect from the provider
            manager.disconnectProvider(providerID);
            
            // Invalidate session
            ExternalContext    externalContext = FacesContext.getCurrentInstance().getExternalContext();
//            HttpServletRequest request         = (HttpServletRequest) externalContext.getRequest();
//            this.invalidateSession(request);
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            
            // Redirect to home page
            FacesContext.getCurrentInstance().getExternalContext().redirect(externalContext.getRequestContextPath() + "home.xhtml");
        } catch (IOException ex) {
            System.out.println("UserSessionBean - IOException: " + ex.toString());
        }
    }
    
	private String getRedirectURL(){
		HttpServletRequest request = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest());
		String file = "/views/socialLoginSuccess.xhtml";
		URL reconstructedURL = null;
		try {
			reconstructedURL = new URL(request.getScheme(),
			                               request.getServerName(),
			                               request.getServerPort(),
			                               file);
		} catch (MalformedURLException e) {logger.error(e.getMessage());}
		return reconstructedURL.toString();
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public String getProviderID() {
		return providerID;
	}

	public void setProviderID(String providerID) {
		this.providerID = providerID;
	}

	public String getOriginalURL() {
		return originalURL;
	}

	public void setOriginalURL(String originalURL) {
		if("".equals(originalURL)){
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			originalURL = request.getRequestURL().toString(); 
		}
		this.originalURL = originalURL;
	}

	public SocialAuthManager getManager() {
		return manager;
	}

	public void setManager(SocialAuthManager manager) {
		this.manager = manager;
	}
    
    // Getters and Setters
    
}