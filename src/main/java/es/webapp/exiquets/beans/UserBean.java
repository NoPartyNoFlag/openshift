package es.webapp.exiquets.beans;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.domain.entities.Usuariosanonimo;
import es.webapp.exiquets.service.MailService;
import es.webapp.exiquets.service.RolesService;
import es.webapp.exiquets.service.UsuariosAnonimosService;
import es.webapp.exiquets.service.UsuariosService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class UserBean {
	
	@Inject private UsuariosAnonimosService gestorUsuariosAnonimos;
	@Inject private UsuariosService gestorUsuarios;
	@Inject private RolesService gestorRoles;
	
	@Inject private AuthenticationManager authenticationManager = null;
	@Inject private MailService mailService;
	
	private FacesContext fCtx = FacesContext.getCurrentInstance();
	private ResourceBundle msg = fCtx.getApplication().evaluateExpressionGet(fCtx, "#{msg}", ResourceBundle.class);

	private User usuarioRegistrado = new User();
	private Usuariosanonimo usuarioAnonimo = new Usuariosanonimo();
	private boolean usuarioLogueado = false;
	
	private String timeZoneOffset;


	@PostConstruct
	private void initBean(){
		HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		String sessionId = session.getId();
		usuarioAnonimo = gestorUsuariosAnonimos.getUsuarioPorSession(sessionId);
		usuarioLogueado = false;
	}

	public void actualizaUsuario(){
		gestorUsuarios.updateEntity(usuarioRegistrado);
	}
	
	public String registraUsuario(){
		String result = msg.getString("login.info.usuarioRegistrado");
		gestorUsuarios.saveEntity(usuarioRegistrado);
		UserRole userRole = new UserRole();
		userRole.setUser(usuarioRegistrado);
		userRole.setAuthority("ROLE_USER");
		gestorRoles.saveEntity(userRole);
//		usuarioRegistrado.getUserRoles().add(userRole);
		try{
			mailService.sendMail("admin@app-exiquets.rhcloud.com", usuarioRegistrado.getMail(), "Registro exiquets - No responder al mail", "Haga click en el enlace para activar su cuenta: " + getActivationURL());
		}catch(Exception e){
			result = msg.getString("login.error.usuarioRegistrado");
		}

		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(result));
		return "/views/login";
	}
	
	private String getActivationURL(){
		HttpServletRequest request = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest());
		String file = "/views/activacionUsuario.xhtml?mailUsuario=" + usuarioRegistrado.getMail() + "&idRegistro=" + usuarioRegistrado.getPassword();
		URL reconstructedURL = null;
		try {
			reconstructedURL = new URL(request.getScheme(),
			                               request.getServerName(),
			                               request.getServerPort(),
			                               file);
		} catch (MalformedURLException e) {}
		return reconstructedURL.toString();
	}
	
	private String getRecuperacionURL(){
		User userFilter = new User();
		userFilter.setMail(usuarioRegistrado.getMail());
		User user = gestorUsuarios.getUsuarioFiltrado(userFilter);
		HttpServletRequest request = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest());
		String file = "/views/restorePassword.xhtml?mailUsuario=" + user.getMail() + "&idRegistro=" + user.getPassword();
		URL reconstructedURL = null;
		try {
			reconstructedURL = new URL(request.getScheme(),
			                               request.getServerName(),
			                               request.getServerPort(),
			                               file);
		} catch (MalformedURLException e) {}
		return reconstructedURL.toString();
	}
	
	public String recuperarPassword(){
		String result = msg.getString("login.info.passwordRestored");
		try{
			mailService.sendMail("admin@app-exiquets.rhcloud.com", usuarioRegistrado.getMail(), "Registro exiquets - No responder al mail", "Haga click en el enlace para activar su cuenta: " + getRecuperacionURL());
		}catch(Exception e){
			result = msg.getString("login.error.passwordRestored");
		}
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(result));
		return "/views/login";
	}

	public String login() {
		String viewReturn = "/views/login";
		String resultMessage = msg.getString("login.error.badCredentials");

		Authentication request = new UsernamePasswordAuthenticationToken(this.usuarioRegistrado.getMail(), this.usuarioRegistrado.getPassword());
		Authentication result = null;
		try{
			result = authenticationManager.authenticate(request);
		}catch(AuthenticationException e){
			resultMessage = msg.getString("login.error.badCredentials");
		}
		SecurityContextHolder.getContext().setAuthentication(result);
		
		if(result != null && result.isAuthenticated()){
			usuarioAnonimo = null;
			User userFilter = new User();
			userFilter.setMail(((org.springframework.security.core.userdetails.User)result.getPrincipal()).getUsername());
			usuarioRegistrado = gestorUsuarios.getUsuarioFiltrado(userFilter);
			usuarioLogueado = true;
			
			resultMessage = msg.getString("login.info.successLogin");
			viewReturn = "/views/privado/modificarUsuario";
		}

		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultMessage));
		return viewReturn;
	}
	
	public String cancel() {
        return null;
    }
	
    public AuthenticationManager getAuthenticationManager() {
        return authenticationManager;
    }
 
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

	public String getNombre(){
		return usuarioRegistrado != null?usuarioRegistrado.getNombre():"";
	}
	
	public void setNombre(String nombre){
		this.usuarioRegistrado.setNombre(nombre);
	}
	public String getMail(){
		return usuarioRegistrado != null?usuarioRegistrado.getMail():"";
	}
	
	public void setMail(String mail){
		this.usuarioRegistrado.setMail(mail);
	}
	public String getPassword(){
		return usuarioRegistrado != null?usuarioRegistrado.getPassword():"";
	}
	
	public void setPassword(String password){
		this.usuarioRegistrado.setPassword(password);
	}

	public User getUsuarioRegistrado() {
		return usuarioRegistrado;
	}

	public void setUsuarioRegistrado(User usuarioRegistrado) {
		this.usuarioRegistrado = usuarioRegistrado;
	}

	public Usuariosanonimo getUsuarioAnonimo() {
		return usuarioAnonimo;
	}

	public void setUsuarioAnonimo(Usuariosanonimo usuarioAnonimo) {
		this.usuarioAnonimo = usuarioAnonimo;
	}

	public String getId() {
		return usuarioLogueado?usuarioRegistrado.getNombre():usuarioAnonimo.getIdSesion();
	}

	public void setId(String id) {}

	public boolean isUsuarioLogueado() {
		return usuarioLogueado;
	}

	public void setUsuarioLogueado(boolean usuarioLogueado) {
		this.usuarioLogueado = usuarioLogueado;
	}

	public String getTimeZoneOffset() {
		return timeZoneOffset;
	}

	public void setTimeZoneOffset(String timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}

}
