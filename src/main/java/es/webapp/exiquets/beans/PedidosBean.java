package es.webapp.exiquets.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.EstadoPedido;
import es.webapp.exiquets.domain.entities.Pedido;
import es.webapp.exiquets.domain.entities.PedidoHasProducto;
import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.service.EstadosPedidoService;
import es.webapp.exiquets.service.PedidosService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class PedidosBean {
	
	@Inject private PedidosService gestorPedidos;
	@Inject private ProductosBean productoBean;
	@Inject private EstadosPedidoService gestorEstadosPedido;
	
	private LazyDataModel<Pedido> listaPedidos;
	private List<PedidoHasProducto> listaProductos = new ArrayList<PedidoHasProducto>();
	private Pedido pedidoSeleccionado;
	private PedidoHasProducto productoSeleccionado;
	private User usuarioPedido;
	private Date from;
	private Date to;

	private ResourceBundle msg;

	
	@PostConstruct
	public void init(){
		listaPedidos = new LazyDataModel<Pedido>(){
			private static final long serialVersionUID = 1L;
			@Override
			public List<Pedido> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				if(from != null){
					filters.put("from", from);
				}
				if(to != null){
					filters.put("to", to);
				}
				if(usuarioPedido != null){
					filters.put("userId", usuarioPedido.getUserId());
				}
				listaPedidos.setRowCount(gestorPedidos.countPedidos(filters));
				return gestorPedidos.getPedidos(first, pageSize, sortField, sortOrder, filters);
			}
		};
		int totalRowCount = gestorPedidos.countPedidos(null);
		listaPedidos.setRowCount(totalRowCount);
		
	}
	
	public void guardaPedidos(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, msg.getString("pedidosUsuarios.popup.titulo"), msg.getString("pedidosUsuarios.popup.guardado"));
		for(Pedido p : (List<Pedido>)listaPedidos.getWrappedData()){
			EstadoPedido ep = gestorEstadosPedido.getById(p.getEstadoPedido().getIdEstadoPedido());
			p.setEstadoPedido(ep);
			gestorPedidos.updateEntity(p);
		}
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public void eliminarPedido(Pedido p){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("pedidosUsuarios.popup.pedidoEliminado");
		String tituloString = msg.getString("pedidosUsuarios.popup.titulo");
		FacesMessage result = new FacesMessage(FacesMessage.SEVERITY_INFO, tituloString, resultString);
		p = gestorPedidos.getById(p.getIdPedido());
		for(PedidoHasProducto producto : p.getPedidoHasProductos()){
			gestorPedidos.deletePedidosHasProductos(producto);
		}
		gestorPedidos.deleteEntity(p);
		FacesContext.getCurrentInstance().addMessage(null, result);
	}
	
	public LazyDataModel<Pedido> getListaPedidos() {
		return listaPedidos;
	}


	public void setListaPedidos(LazyDataModel<Pedido> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}


	public Date getFrom() {
		return from;
	}


	public void setFrom(Date from) {
		this.from = from;
	}


	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}


	public User getUsuarioPedido() {
		return usuarioPedido;
	}


	public void setUsuarioPedido(User usuarioPedido) {
		this.usuarioPedido = usuarioPedido;
	}
	
	public Pedido getPedidoSeleccionado() {
		return pedidoSeleccionado;
	}

	public void setPedidoSeleccionado(Pedido pedidoSeleccionado) {
		if(pedidoSeleccionado != null){
			this.pedidoSeleccionado = gestorPedidos.getById(pedidoSeleccionado.getIdPedido());
			this.listaProductos = gestorPedidos.getPedidoHasProductoCompletoById(this.pedidoSeleccionado.getIdPedido());
		}
	}
	
	public List<PedidoHasProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<PedidoHasProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public PedidoHasProducto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(PedidoHasProducto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}
	
	public void goToDetallePedido() throws IOException{
		FacesContext.getCurrentInstance().getExternalContext().redirect("/views/privado/detallePedidoCliente.xhtml");
	}
	
	public void goToDetalleProducto() throws IOException{
		productoBean.setProductoSeleccionado(productoSeleccionado.getProducto());
		FacesContext.getCurrentInstance().getExternalContext().redirect("/views/privado/detalleProducto.xhtml");
	}
	
	public String goToPedidosUsuarios(){
		return "/views/admin/pedidosUsuarios";
	}
	
	public String goToPedidosCliente(){
		return "/views/privado/pedidosCliente";
	}
	
}
