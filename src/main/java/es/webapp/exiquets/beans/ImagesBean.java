package es.webapp.exiquets.beans;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.webforms.AddProductoForm;
import es.webapp.exiquets.service.ProductosService;

@Named
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ImagesBean {

	
	@Inject private ProductosService gestorProductos;
	@Inject private CarroCompraBean carroCompraBean;
	
	private List<Producto> listaProductos;
	
	@PostConstruct
	public void init(){
		listaProductos = gestorProductos.getProductosTodosIdiomas();
	}
	
	public void setImagen(StreamedContent img){

    }
    
    public String getImagen(int idProducto){
    	Producto productoRequest = gestorProductos.getById(idProducto);
    	return gestorProductos.getImagenDeDisco(productoRequest.getImg());
    }
    
    public void goToDetalleProducto(){
    	String idProducto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idProductoSeleccionado");
		if(!"".equals(idProducto) && StringUtils.isNumeric(idProducto)){
			try {
				goToDetalleProducto(Integer.parseInt(idProducto));
			} catch (NumberFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
    
    public void goToDetalleProducto(int idProducto) throws IOException{
    	AddProductoForm productoForm = new AddProductoForm();
    	productoForm.setProducto(gestorProductos.getById(idProducto));
    	productoForm.setCantidad(1);
    	carroCompraBean.setProductoSeleccionado(productoForm);
    	FacesContext.getCurrentInstance().getExternalContext().redirect("/views/detalleProducto.xhtml");
    }

	public List<Producto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}
    
    
}
