package es.webapp.exiquets.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Direccion;
import es.webapp.exiquets.domain.entities.EstadoPedido;
import es.webapp.exiquets.domain.entities.Pedido;
import es.webapp.exiquets.domain.entities.PedidoHasProducto;
import es.webapp.exiquets.domain.entities.PedidoHasProductoPK;
import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.UsersHasProducto;
import es.webapp.exiquets.domain.entities.UsersHasProductoPK;
import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProducto;
import es.webapp.exiquets.domain.entities.UsuarioanonimosHasProductoPK;
import es.webapp.exiquets.domain.entities.Usuariosanonimo;
import es.webapp.exiquets.domain.webforms.AddProductoForm;
import es.webapp.exiquets.domain.webforms.ProductoForm;
import es.webapp.exiquets.service.DireccionService;
import es.webapp.exiquets.service.PedidosService;
import es.webapp.exiquets.service.ProductosService;
import es.webapp.exiquets.service.UsersHasProductoService;
import es.webapp.exiquets.service.UsuarioAnonimoHasProductoServiceImpl;
import es.webapp.exiquets.service.UsuariosAnonimosService;
import es.webapp.exiquets.service.UsuariosService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class CarroCompraBean{
	
	@Inject private UserBean userBean;
	
	@Inject private ProductosService gestorProductos;
	@Inject private UsuariosService gestorUsuarios;
	@Inject private UsuariosAnonimosService gestorUsuariosAnonimos;
	@Inject private PedidosService gestorPedidos;
	@Inject private UsersHasProductoService gestorUsersHasProducto;
	@Inject private UsuarioAnonimoHasProductoServiceImpl gestorUsuarioAnonimoHasProducto;
	@Inject private DireccionService gestorDirecciones;
	
	private ResourceBundle msg;

//	private List<Producto> listaProductos = new ArrayList<Producto>();
	private List<?> listaProductosUsuario;
	private LazyDataModel<AddProductoForm> listaProductos;
	private AddProductoForm productoSeleccionado;
	private ProductoForm filtroProductos;
	private String campoOrdenacion;
	private Direccion direccionPedidoSeleccionada;
	
	
	private User usuarioRegistrado = null;
	private Usuariosanonimo usuarioAnonimo = null;
	

	@PostConstruct
	private void initBean(){
		listaProductos = new LazyDataModel<AddProductoForm>(){
			private static final long serialVersionUID = 1L;
			@Override
			public List<AddProductoForm> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				
				filters = new HashMap<String,Object>();
				if(filtroProductos.getNombre() != null && !"".equals(filtroProductos.getNombre())){
					filters.put("nombre", filtroProductos.getNombre());
				}
				if(filtroProductos.getDescripcion() != null && !"".equals(filtroProductos.getDescripcion())){
					filters.put("descripcion", filtroProductos.getDescripcion());
				}
				if(filtroProductos.getPrecioMayor() != null && filtroProductos.getPrecioMayor() != 0){
					filters.put("precioMayor", filtroProductos.getPrecioMayor());
				}
				if(filtroProductos.getPrecioMenor() != null && filtroProductos.getPrecioMenor() != 0){
					filters.put("precioMenor", filtroProductos.getPrecioMenor());
				}
				if(filtroProductos.getCategoriaBean() != null && filtroProductos.getCategoriaBean().getIdCategorias() > 0 ){
					filters.put("categoria", filtroProductos.getCategoriaBean().getIdCategorias());
				}
				
				if("precioAsc".equals(campoOrdenacion)){
					sortField = "precio";
					sortOrder = SortOrder.ASCENDING;
				}else if("precioDesc".equals(campoOrdenacion)){
					sortField = "precio";
					sortOrder = SortOrder.DESCENDING;
				}
				
				listaProductos.setRowCount(gestorProductos.countProductos(filters));

				List<Producto> listaProductos = gestorProductos.getProductos(first, pageSize, sortField, sortOrder, filters);
				
				List<AddProductoForm> listaAddProductos = new ArrayList<AddProductoForm>();
				for(Producto p : listaProductos){
					AddProductoForm addProductoForm = new AddProductoForm();
					addProductoForm.setProducto(p);
					addProductoForm.setCantidad(1);
					listaAddProductos.add(addProductoForm);
				}
				return listaAddProductos;
			};
		};
		int totalRowCount = gestorProductos.countProductos(null);
		listaProductos.setRowCount(totalRowCount);

		filtroProductos = new ProductoForm();	
		
		setDefaultAddress();
		updateDatosUsuarios();
	}
	
	public void setDefaultAddress(){
		Direccion filtroDireccion = new Direccion();
		filtroDireccion.setUser(userBean.getUsuarioRegistrado());
		byte activo = 1;
		filtroDireccion.setActiva(activo);
		filtroDireccion.setDefecto(activo);
		List<Direccion> direcciones = gestorDirecciones.getDireccionesFiltradas(filtroDireccion);
		direccionPedidoSeleccionada = !direcciones.isEmpty()?direcciones.get(0):null;
	}
	
	public void updateDatosUsuarios(){
		if(userBean.isUsuarioLogueado()){
			usuarioRegistrado = gestorUsuarios.getById(userBean.getUsuarioRegistrado().getUserId());
//			this.listaProductos = gestorProductos.getProductosCompletos(usuarioRegistrado);
			UsersHasProducto filtro = new UsersHasProducto();
			UsersHasProductoPK usersHasProductoId = new UsersHasProductoPK();
			usersHasProductoId.setUsersUserId(usuarioRegistrado.getUserId());
			filtro.setId(usersHasProductoId);
			listaProductosUsuario = gestorUsersHasProducto.getUsersHasProductoCompleto(filtro); 
		}else{
			usuarioAnonimo = gestorUsuariosAnonimos.getById(userBean.getUsuarioAnonimo().getIdUsuarioAnonimo());
//			this.listaProductos = gestorProductos.getProductosCompletos(usuarioAnonimo);
			listaProductosUsuario = new ArrayList<UsuarioanonimosHasProducto>(usuarioAnonimo.getUsuarioanonimosHasProductos());
		}
	}


//	public List<Producto> getListaProductos(){
//		return this.listaProductos;
//	}
	
	
	public String addProducto(AddProductoForm p){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String tituloResultString = msg.getString("popup.titulo");
		String resultString = msg.getString("addProducto.info.productAdded");
		FacesMessage resultMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,tituloResultString,resultString);
		
		
		Producto producto = gestorProductos.getById(p.getProducto().getIdProductos());
		
		if(userBean.isUsuarioLogueado()){
			usuarioRegistrado = gestorUsuarios.getById(userBean.getUsuarioRegistrado().getUserId());
			UsersHasProducto usersHasProducto = new UsersHasProducto();
			UsersHasProductoPK pk = new UsersHasProductoPK();
			pk.setProductos_idProductos(producto.getIdProductos());
			pk.setUsersUserId(usuarioRegistrado.getUserId());
			
			usersHasProducto.setId(pk);
			
			usersHasProducto.setUser(usuarioRegistrado);
			usersHasProducto.setProducto(producto);
			
			int cantidadTotal = p.getCantidad();
			
			User user = gestorUsuarios.getById(userBean.getUsuarioRegistrado().getUserId());
			Set<UsersHasProducto> usersHasProductosSet = user.getUsersHasProductos();
			if(!usersHasProductosSet.isEmpty()){
				for(UsersHasProducto it : usersHasProductosSet){
					if(it.equals(usersHasProducto)){
						cantidadTotal = cantidadTotal + it.getCantidad();
					}
				}
				if(gestorProductos.checkAndUpdateStock(producto,p.getCantidad())){
					usersHasProducto.setCantidad(cantidadTotal);
					if(cantidadTotal == p.getCantidad()){
						gestorUsersHasProducto.saveEntity(usersHasProducto);
					}else{
						gestorUsersHasProducto.updateEntity(usersHasProducto);					
					}
				}else{
					resultString = msg.getString("addProducto.info.overStock");
					resultMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,tituloResultString,resultString);
				}
			}else{
				if(gestorProductos.checkAndUpdateStock(producto,p.getCantidad())){
					usersHasProducto.setCantidad(cantidadTotal);
					gestorUsersHasProducto.saveEntity(usersHasProducto);
				}else{
					resultString = msg.getString("addProducto.info.overStock");
					resultMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,tituloResultString,resultString);
				}
			}
			gestorUsuarios.getEM().refresh(usuarioRegistrado);
		}else{
			usuarioAnonimo = gestorUsuariosAnonimos.getById(usuarioAnonimo.getIdUsuarioAnonimo());
			UsuarioanonimosHasProducto usuarioAnonimoHasProducto = new UsuarioanonimosHasProducto();
			UsuarioanonimosHasProductoPK pk = new UsuarioanonimosHasProductoPK();
			pk.setProductos_idProductos(producto.getIdProductos());
			pk.setUsuariosAnonimos_idUsuarioAnonimo(usuarioAnonimo.getIdUsuarioAnonimo());
			
			usuarioAnonimoHasProducto.setId(pk);
			usuarioAnonimoHasProducto.setProducto(producto);
			usuarioAnonimoHasProducto.setUsuariosanonimo(usuarioAnonimo);
			
			int cantidadTotal = p.getCantidad();
			
			Set<UsuarioanonimosHasProducto> usersHasProductosSet = usuarioAnonimo.getUsuarioanonimosHasProductos();
			if(!usersHasProductosSet.isEmpty()){
				for(UsuarioanonimosHasProducto it : usersHasProductosSet){
					if(it.equals(usuarioAnonimoHasProducto)){
						cantidadTotal = cantidadTotal + it.getCantidad();
					}
				}
				if(gestorProductos.checkAndUpdateStock(producto,p.getCantidad())){
					usuarioAnonimoHasProducto.setCantidad(cantidadTotal);
					if(cantidadTotal == p.getCantidad()){
						gestorUsuarioAnonimoHasProducto.saveEntity(usuarioAnonimoHasProducto);
					}else{
						gestorUsuarioAnonimoHasProducto.updateEntity(usuarioAnonimoHasProducto);
					}
				}else{
					resultString = msg.getString("addProducto.info.overStock");
					resultMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,tituloResultString,resultString);
				}
			}else{
				if(gestorProductos.checkAndUpdateStock(producto,p.getCantidad())){
					usuarioAnonimoHasProducto.setCantidad(p.getCantidad());	
					gestorUsuarioAnonimoHasProducto.saveEntity(usuarioAnonimoHasProducto);
				}else{
					resultString = msg.getString("addProducto.info.overStock");
					resultMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,tituloResultString,resultString);
				}
			}
			gestorUsuariosAnonimos.getEM().refresh(usuarioAnonimo);
		}
		FacesContext.getCurrentInstance().addMessage(null, resultMessage);
		return updatePage();
	}
	
	public String eliminaProducto(UsersHasProducto usersHasProductos){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("addProducto.info.productDeleted");
		String tituloResultString = msg.getString("popup.titulo");
		usersHasProductos = gestorUsersHasProducto.getUserHasProductoCompleto(usersHasProductos);
		if(userBean.isUsuarioLogueado()){
			Producto producto = usersHasProductos.getProducto();
			producto.setStock(producto.getStock() + usersHasProductos.getCantidad());
			gestorProductos.updateEntity(producto);
			gestorUsersHasProducto.deleteEntity(usersHasProductos);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tituloResultString,resultString));
		}
//		gestorUsuarios.getEM().refresh(usuarioRegistrado);
		return updatePage();
	}
	
	public String eliminaProducto(UsuarioanonimosHasProducto usarioAnonimoHasProducto){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("addProducto.info.productDeleted");
		String tituloResultString = msg.getString("popup.titulo");
		usarioAnonimoHasProducto = gestorUsuarioAnonimoHasProducto.getById(usarioAnonimoHasProducto.getId());
		Producto producto = usarioAnonimoHasProducto.getProducto();
		producto.setStock(producto.getStock() + usarioAnonimoHasProducto.getCantidad());
		gestorProductos.updateEntity(producto);
		gestorUsuarioAnonimoHasProducto.deleteEntity(usarioAnonimoHasProducto);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tituloResultString,resultString));
//		gestorUsuariosAnonimos.getEM().refresh(usuarioAnonimo);
		return updatePage();
	}
	
	public String realizarPedido(){
		usuarioRegistrado = gestorUsuarios.getById(usuarioRegistrado.getUserId());
		
		Pedido pedido = new Pedido();
		EstadoPedido estado = new EstadoPedido();
		estado.setIdEstadoPedido(1);
		pedido.setEstadoPedido(estado);
		pedido.setFechaPedido(new Date());
		pedido.setUser(usuarioRegistrado);
		
		pedido.setDireccion(direccionPedidoSeleccionada);
		
		gestorPedidos.saveEntity(pedido);
		
//		pedido = gestorPedidos.getById(pedido.getIdPedido());
		
//		Set<PedidoHasProducto> productosPedidos = new HashSet<PedidoHasProducto>();
		
		for(UsersHasProducto userHasProducto : usuarioRegistrado.getUsersHasProductos()){
			Producto producto = gestorProductos.getById(userHasProducto.getProducto().getIdProductos());
			
			PedidoHasProducto pedidoHasProducto = new PedidoHasProducto();
			
			PedidoHasProductoPK id = new PedidoHasProductoPK();
			id.setPedido_idPedido(pedido.getIdPedido());
			id.setProductos_idProductos(producto.getIdProductos());
			
			pedidoHasProducto.setId(id);
			
			pedidoHasProducto.setCantidad(userHasProducto.getCantidad());
			pedidoHasProducto.setPedido(pedido);
			pedidoHasProducto.setProducto(producto);
			
			gestorPedidos.savePedidosHasProductos(pedidoHasProducto);
//			No seria necesario
//			productosPedidos.add(pedidoHasProducto);
		
		}
		
		
		usuarioRegistrado.addPedido(pedido);
		gestorUsuarios.updateEntity(usuarioRegistrado);
		
		
//		pedido.setPedidoHasProductos(productosPedidos);
		
//		Si todo va bien, vaciar carrito
		for(UsersHasProducto userHasProducto : usuarioRegistrado.getUsersHasProductos()){
			gestorUsersHasProducto.deleteEntity(userHasProducto);
		}
		gestorUsuarios.getEM().refresh(usuarioRegistrado);
		
		return updatePage();
	}	

	public float getTotalFactura(){
		float total = 0;
		for(UsersHasProducto producto : (List<UsersHasProducto>)listaProductosUsuario){
			total += (producto.getProducto().getPrecio() * producto.getCantidad());
		}
		return total;
	}

	public List<?> getListaProductosUsuario() {
		return listaProductosUsuario;
	}

	public void setListaProductosUsuario(List<?> listaProductosUsuario) {
		this.listaProductosUsuario = listaProductosUsuario;
	}



	public LazyDataModel<AddProductoForm> getListaProductos() {
		return listaProductos;
	}


	public void setListaProductos(LazyDataModel<AddProductoForm> listaProductos) {
		this.listaProductos = listaProductos;
	}
	
	
	
	public AddProductoForm getProductoSeleccionado() {
		return productoSeleccionado;
	}


	public void setProductoSeleccionado(AddProductoForm productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}


	public ProductoForm getFiltroProductos() {
		return filtroProductos;
	}


	public void setFiltroProductos(ProductoForm filtroProductos) {
		this.filtroProductos = filtroProductos;
	}

	public String getCampoOrdenacion() {
		return campoOrdenacion;
	}


	public void setCampoOrdenacion(String campoOrdenacion) {
		this.campoOrdenacion = campoOrdenacion;
	}

	public Direccion getDireccionPedidoSeleccionada() {
		return direccionPedidoSeleccionada;
	}

	public void setDireccionPedidoSeleccionada(Direccion direccionPedidoSeleccionada) {
		this.direccionPedidoSeleccionada = direccionPedidoSeleccionada;
	}

	public void goToDetalleProducto() throws IOException{
		FacesContext.getCurrentInstance().getExternalContext().redirect("/views/detalleProducto.xhtml?productoSeleccionado=" + this.productoSeleccionado.getProducto().getIdProductos());
	}
	
	public void goToDetalleProducto(AddProductoForm producto) throws IOException{
		this.setProductoSeleccionado(producto);
		FacesContext.getCurrentInstance().getExternalContext().redirect("/views/detalleProducto.xhtml");
	}


	public String updatePage(){
//		return "/views/addProductos?faces-redirect";
//		initBean();
		updateDatosUsuarios();
		return "/views/privado/detalleCarroCompra";
	}

}
