package es.webapp.exiquets.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class FileDownloadBean{
    private StreamedContent file;
    
    public StreamedContent getFile() throws FileNotFoundException {
    	String logDir = System.getenv("OPENSHIFT_LOG_DIR");
    	if(logDir == null){
    		logDir = System.getProperty("OPENSHIFT_LOG_DIR");
    	}
    	File f = new File(logDir,"jbossews.log");
    	InputStream inputStream = new FileInputStream(f);
        file = new DefaultStreamedContent(inputStream, "text/plain", f.getName());
        return file;
    }
	
}
