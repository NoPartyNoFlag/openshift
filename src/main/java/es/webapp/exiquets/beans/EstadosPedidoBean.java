package es.webapp.exiquets.beans;

import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.EstadoPedido;
import es.webapp.exiquets.service.EstadosPedidoService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class EstadosPedidoBean{
	
	@Inject private EstadosPedidoService gestorEstadosPedido;
	
	private ResourceBundle msg;
	
	private List<EstadoPedido> listaEstadosPedido;
	
	@PostConstruct
	public void initBean(){
		listaEstadosPedido = gestorEstadosPedido.findAll();
	}
	
	public String getEstadoPedidoString(int idEstadoPedido){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		return msg.getString("estadoPedido." + idEstadoPedido);
	}
	
	public List<EstadoPedido> getListaEstadosPedido() {
		return listaEstadosPedido;
	}
}
