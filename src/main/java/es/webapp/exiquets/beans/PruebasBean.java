package es.webapp.exiquets.beans;



import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class PruebasBean{
	
	private String text;
	 
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    
    public void handleKeyEvent() {
        text = text.toUpperCase();
    }
    
    private String firstname;
    private String lastname;
 
    public String getFirstname() {
        return firstname;
    }
 
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
 
    public String getLastname() {
        return lastname;
    }
 
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
 
    public void save() {
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage("Welcome " + firstname + " " + lastname));
    }

}