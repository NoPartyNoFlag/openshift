package es.webapp.exiquets.beans;

import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Direccion;
import es.webapp.exiquets.domain.entities.Tipodireccion;
import es.webapp.exiquets.service.DireccionService;
import es.webapp.exiquets.service.TipoDireccionService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class DireccionesBean {

	
	@Inject private DireccionService gestorDirecciones;
	@Inject private TipoDireccionService gestorTipoDirecciones;
	@Inject private UserBean usuario;
	
	List<Direccion> listaDirecciones;
	List<Tipodireccion> listaTiposDireccion;
	Direccion direccionDefecto;
	private ResourceBundle msg;
	private Direccion filtro = new Direccion();
	
	@PostConstruct
	private void init(){
		filtro.setUser(usuario.getUsuarioRegistrado());
		byte activa = 1;
		filtro.setActiva(activa);
		listaDirecciones = gestorDirecciones.getDireccionesFiltradas(filtro);
		
		listaTiposDireccion = gestorTipoDirecciones.findAll();
		
		setDefaultAddress();
	}
	
	public void setDefaultAddress(){
		Direccion filtroDireccion = new Direccion();
		filtroDireccion.setUser(usuario.getUsuarioRegistrado());
		byte activo = 1;
		filtroDireccion.setActiva(activo);
		filtroDireccion.setDefecto(activo);
		List<Direccion> direcciones = gestorDirecciones.getDireccionesFiltradas(filtroDireccion);
		direccionDefecto = !direcciones.isEmpty()?direcciones.get(0):null;
	}
	
	public void addDireccion(){
		Direccion direccion = new Direccion();
		direccion.setTipodireccion(gestorTipoDirecciones.getById(1));
		direccion.setUser(usuario.getUsuarioRegistrado());
		byte activa = 1;
		direccion.setActiva(activa);
		gestorDirecciones.saveEntity(direccion);
		init();
	}
	
	public void guardaDirecciones(){
		for(Direccion d : listaDirecciones){
			if(d.equals(direccionDefecto)){
				byte defecto = 1;
				d.setDefecto(defecto);
			}else{
				byte defecto = 0;
				d.setDefecto(defecto);
			}
			gestorDirecciones.updateEntity(d);
		}
		init();
	}
	public void eliminarDireccion(Direccion direccion){
		direccion = gestorDirecciones.getById(direccion.getIdDireccion());
		byte activa = 0;
		direccion.setActiva(activa);
		gestorDirecciones.updateEntity(direccion);
		init();
	}

	public List<Direccion> getListaDirecciones() {
		return listaDirecciones;
	}

	public void setListaDirecciones(List<Direccion> listaDirecciones) {
		this.listaDirecciones = listaDirecciones;
	}
	
	public Direccion getFiltro() {
		return filtro;
	}

	public void setFiltro(Direccion filtro) {
		this.filtro = filtro;
	}
	
	public List<Tipodireccion> getListaTiposDireccion() {
		return listaTiposDireccion;
	}

	public void setListaTiposDireccion(List<Tipodireccion> listaTiposDireccion) {
		this.listaTiposDireccion = listaTiposDireccion;
	}
	
	public Direccion getDireccionDefecto() {
		return direccionDefecto;
	}

	public void setDireccionDefecto(Direccion direccionDefecto) {
		this.direccionDefecto = direccionDefecto;
	}

	public String getTipoDireccionString(int id){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		return msg.getString("direccion.tipoDireccion." + id);
	}
	
	public String goToListaDirecciones(){
		return "/views/privado/direccionesCliente.xhtml";
	}
	
}
