package es.webapp.exiquets.beans.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.service.IdiomasService;

@Named
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class IdiomaConverter implements Converter {
	@Inject private IdiomasService gestorIdiomas;
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.hasText(value)) {
			return gestorIdiomas.getById(Integer.parseInt(value));
		}
		return null;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Idioma) {
			return String.valueOf(((Idioma)value).getIdIdiomas());
		}
		return null;
	}
}
