package es.webapp.exiquets.beans.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.EstadoPedido;
import es.webapp.exiquets.service.EstadosPedidoService;

@Named
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class EstadoPedidoConverter implements Converter {
	@Inject private EstadosPedidoService gestorPedido;
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.hasText(value)) {
			return gestorPedido.getById(Integer.parseInt(value));
		}
		return null;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof EstadoPedido) {
			return String.valueOf(((EstadoPedido)value).getIdEstadoPedido());
		}
		return null;
	}
}
