package es.webapp.exiquets.beans.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.beans.UserBean;

@Named
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class DatesConverter implements Converter  {
	private SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
	@Inject private UserBean userBean;
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Date date = new Date();
		try {
			date = isoFormat.parse(arg2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		
		Date fecha = (Date)arg2;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		
		int timeZoneOffsetClient = (Integer.parseInt(userBean.getTimeZoneOffset())/60)*-1;
		int timeZoneOffsetServer = cal.getTimeZone().getRawOffset()/(1000*60*60);
		if(TimeZone.getDefault().inDaylightTime( new Date() )){
			timeZoneOffsetServer += 1;
		}
		
		cal.add(Calendar.HOUR_OF_DAY, timeZoneOffsetClient - timeZoneOffsetServer);
		fecha = cal.getTime();

		return isoFormat.format(fecha);
	}

}
