package es.webapp.exiquets.beans.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.service.ProductosService;

@Named
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class ProductoConverter implements Converter {
	@Inject private ProductosService gestorProductos;
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.hasText(value)) {
			return gestorProductos.getById(Integer.parseInt(value));
		}
		return null;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Producto) {
			return String.valueOf(((Producto)value).getIdProductos());
		}
		return null;
	}
}
