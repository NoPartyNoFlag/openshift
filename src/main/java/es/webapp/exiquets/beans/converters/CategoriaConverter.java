package es.webapp.exiquets.beans.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.service.CategoriasService;

@Named
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class CategoriaConverter implements Converter {
	@Inject private CategoriasService gestorCategorias;
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.hasText(value)) {
			return gestorCategorias.getById(Integer.parseInt(value));
		}
		return null;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Categoria) {
			return String.valueOf(((Categoria)value).getIdCategorias());
		}
		return null;
	}
}
