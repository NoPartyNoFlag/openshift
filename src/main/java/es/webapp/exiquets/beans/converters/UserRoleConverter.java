package es.webapp.exiquets.beans.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.service.RolesService;

@Named
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class UserRoleConverter implements Converter {
	@Inject private RolesService gestorRoles;
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (StringUtils.hasText(value)) {
			return gestorRoles.getById(Integer.parseInt(value));
		}
		return null;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof UserRole) {
			return String.valueOf(((UserRole)value).getUserRoleId());
		}
		return null;
	}
}
