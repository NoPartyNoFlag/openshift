package es.webapp.exiquets.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.domain.entities.Nombrescategoria;
import es.webapp.exiquets.service.CategoriasService;
import es.webapp.exiquets.service.IdiomasService;
import es.webapp.exiquets.service.NombresCategoriaService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class CategoriasBean{
	
	@Inject private CategoriasService gestorCategorias;
	@Inject private NombresCategoriaService gestorNombresCategorias;
	@Inject private IdiomasService gestorIdiomas;
	
	
	private ResourceBundle msg;
	
	List<Categoria> listaCategorias = new ArrayList<Categoria>();
	List<Nombrescategoria> listaNombresCategoria = new ArrayList<Nombrescategoria>();
	private Categoria categoriaSeleccionada = null;
	
	@PostConstruct
	public void initBean(){
		listaCategorias = gestorCategorias.getCategoriasTodosIdiomas();
	}
	
	public String guardarCategorias(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("categorias.info.categoriasGuardadas");
		for(Categoria c : listaCategorias){
			gestorCategorias.updateEntity(c);
		}
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadCategoriasPage();
	}
	
	public String guardaCategoria(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("detalleCategoria.info.nombresCategoriasGuardadas");
		for(Nombrescategoria nombre : listaNombresCategoria){
			gestorNombresCategorias.updateEntity(nombre);
		}
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadCategoriaPage();
	}
	
	public String crearCategoria(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("categorias.info.categoriaCreada");
		
		List<Idioma> listaIdiomas = gestorIdiomas.findAll();
		if(listaIdiomas.size() > 0){
			Categoria categoria = new Categoria();
			gestorCategorias.saveEntity(categoria);
			for(Idioma idioma : listaIdiomas){
				Nombrescategoria nc = new Nombrescategoria();
				nc.setIdioma(idioma);
				nc.setLiteralCategoria("");
				nc.getCategorias().add(categoria);
				gestorNombresCategorias.saveEntity(nc);
			}
		}

		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadCategoriasPage();
	}
	
	public String eliminarCategoria(Categoria c){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("categorias.info.categoriaEliminada");
		c = gestorCategorias.getById(c.getIdCategorias());
		for(Nombrescategoria nombre : c.getNombrescategorias()){
			gestorNombresCategorias.deleteEntity(nombre);
		}
		gestorCategorias.deleteEntity(c);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadCategoriasPage();
	}
	
	public List<Categoria> getListaCategorias(){
		return listaCategorias;
	}
	
	public List<Nombrescategoria> getListaNombresCategoria(){
		return listaNombresCategoria;
	}
	
	public Categoria getCategoriaSeleccionada() {
		return categoriaSeleccionada;
	}

	public void setCategoriaSeleccionada(Categoria categoria) {
		if(categoria != null){
			this.categoriaSeleccionada = gestorCategorias.getCategoriaTodosIdiomas(categoria.getIdCategorias());
			this.listaNombresCategoria = new ArrayList<Nombrescategoria>(this.categoriaSeleccionada.getNombrescategorias());
		}
	}

	public void goToDetalleCategoria() throws IOException{
		FacesContext.getCurrentInstance().getExternalContext().redirect("/views/admin/detalleCategoria.xhtml");
	}
	
	public String reloadCategoriaPage(){
		setCategoriaSeleccionada(this.categoriaSeleccionada);
		initBean();
		return "/views/admin/detalleCategoria";
	}
	
	public String reloadCategoriasPage(){
		initBean();
		return "/views/admin/categorias";
	}
}
