package es.webapp.exiquets.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.service.RolesService;
import es.webapp.exiquets.service.UsuariosService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class UsuariosRegistradosBean {

	
	@Inject private UsuariosService gestorUsuarios;
	@Inject private RolesService gestorRoles;
	@Inject private UserBean sessionUserBean;
	
	private ResourceBundle msg;
	
	private List<UserRole> listaRoles = new ArrayList<UserRole>();
	private List<User> listaUsuarios = new ArrayList<User>();
	private List<UserRole> rolesUsuario;
	private User usuarioSeleccionado;
	

	@PostConstruct
	public void initBean(){
		listaUsuarios = gestorUsuarios.findAll();
		listaUsuarios.remove(sessionUserBean.getUsuarioRegistrado());
		listaRoles = gestorRoles.getGroupedRoles();
	}
	
	public String guardaUsuarios(){
		for(User u : listaUsuarios){
			gestorUsuarios.updateEntity(u);
		}
		return reloadPage();
	}

	public String eliminaUsuario(User u){
		u = gestorUsuarios.getUsuarioFiltrado(u);
		byte e = 0;
		u.setEnabled(e);
		gestorUsuarios.updateEntity(u);
		return reloadPage();
	}
	
	public String addRole(UserRole role){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("usuarios.info.roleAdded");
		UserRole userRole = new UserRole();
		userRole.setUser(this.usuarioSeleccionado);
		userRole.setAuthority(role.getAuthority());
		gestorRoles.saveEntity(userRole);
//		this.usuarioSeleccionado.getUserRoles().add(role);
//		gestorUsuarios.updateEntity(this.usuarioSeleccionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPageRoles();
	}
	
	public String deleteRole(UserRole role){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("usuarios.info.roleDeleted");
		role = gestorRoles.getById(role.getUserRoleId());
		gestorRoles.deleteEntity(role);
//		this.usuarioSeleccionado.getUserRoles().remove(role);
//		gestorUsuarios.updateEntity(this.usuarioSeleccionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPageRoles();
	}
    
    public List<User> getListaUsuarios(){
    	return listaUsuarios;
    }
    
    public User getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(User usuarioSeleccionado) {
		if(usuarioSeleccionado != null){
			this.usuarioSeleccionado = gestorUsuarios.getUsuarioFiltrado(usuarioSeleccionado);
			rolesUsuario = new ArrayList<UserRole>(this.usuarioSeleccionado.getUserRoles());
		}
	}
	
	public List<UserRole> getListaRoles() {
		return listaRoles;
	}


	public void setListaRoles(List<UserRole> listaRoles) {
		this.listaRoles = listaRoles;
	}
	
	public List<UserRole> getRolesUsuario() {
		return rolesUsuario;
	}

	public void setRolesUsuario(List<UserRole> rolesUsuario) {
		this.rolesUsuario = rolesUsuario;
	}

	public void goToDetalleUsuario() throws IOException{
		FacesContext.getCurrentInstance().getExternalContext().redirect("/views/admin/addRoles.xhtml");
	}
	
	public String reloadPageRoles(){
		setUsuarioSeleccionado(this.usuarioSeleccionado);
    	return "/views/admin/usuarios";
	}

	public String reloadPage(){
    	initBean();
    	return "/views/admin/usuarios";
    }
}