/**
 * 
 */
package es.webapp.exiquets.beans;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.service.IdiomasService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class LanguageBean{
	
	@Inject private IdiomasService gestorIdiomas;
	
	private ResourceBundle msg;

	private Idioma idiomaSeleccionado = new Idioma();
	private List<Idioma> idiomas;
	
	@PostConstruct
	public void initBean(){
		idiomas = gestorIdiomas.findAll();
		//Idioma por defecto
		idiomaSeleccionado.setIdiomaCode("es");
		idiomaSeleccionado.setLocaleCode("es_ES");
	}

	public List<Idioma> getIdiomas() {
		return idiomas;
	}

	public void setIdiomas(List<Idioma> idiomas) {
		this.idiomas = idiomas;
	}

	public Idioma getIdiomaSeleccionado() {
		return idiomaSeleccionado;
	}

	public void setIdiomaSeleccionado(Idioma idiomaSeleccionado) {
		this.idiomaSeleccionado = idiomaSeleccionado;
	}

	public void changeLocale(){
		FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(idiomaSeleccionado.getIdiomaCode()));
	}

	public String addIdioma(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("idiomas.info.idiomaCreado");
		Idioma i = new Idioma();
		gestorIdiomas.saveEntity(i);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPage();
	}
	
	public String guardaIdiomas(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("idiomas.info.idiomasGuardados");
		for(Idioma i : idiomas){
			if(!"".equalsIgnoreCase(i.getIdioma())){
				gestorIdiomas.updateEntity(i);
			}
		}
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPage();
	}
	
	public String eliminarIdioma(Idioma i){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("idiomas.info.idiomaEliminado");
		i = gestorIdiomas.getById(i.getIdIdiomas());
		gestorIdiomas.deleteEntity(i);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPage();
	}
	
	private String reloadPage(){
		initBean();
		return "/views/admin/idiomas";
	}
	
	
}
