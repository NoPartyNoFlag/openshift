package es.webapp.exiquets.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.IOUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.Categoria;
import es.webapp.exiquets.domain.entities.Descripcion;
import es.webapp.exiquets.domain.entities.Idioma;
import es.webapp.exiquets.domain.entities.Nombre;
import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.service.CategoriasService;
import es.webapp.exiquets.service.DescripcionesService;
import es.webapp.exiquets.service.IdiomasService;
import es.webapp.exiquets.service.NombresService;
import es.webapp.exiquets.service.ProductosService;

@Named
@Scope(WebApplicationContext.SCOPE_SESSION)
public class ProductosBean{

	@Inject private ProductosService gestorProductos;
	@Inject private CategoriasService gestorCategorias;
	@Inject private NombresService gestorNombres;
	@Inject private IdiomasService gestorIdiomas;
	@Inject private DescripcionesService gestorDescripciones;


	private ResourceBundle msg;


	private LazyDataModel<Producto> listaProductos;
	private Producto productoSeleccionado;
	private List<Descripcion> descripcionesProducto = null;
	private List<Nombre> nombresProducto = null;
	private UploadedFile uploadedFile;


	@PostConstruct
	public void initBean(){
		listaProductos = new LazyDataModel<Producto>(){
			private static final long serialVersionUID = 1L;
			@Override
			public List<Producto> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				listaProductos.setRowCount(gestorProductos.countProductos(filters));

				return gestorProductos.getProductos(first, pageSize, sortField, sortOrder, filters);
			};
		};
		int totalRowCount = gestorProductos.countProductos(null);
		listaProductos.setRowCount(totalRowCount);
	}


	//	Gestión de todos los productos
	public String guardaProductos(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		for(Producto p : ((List<Producto>)listaProductos.getWrappedData())){
			if(p.getIdProductos() != 0){
				
				Categoria c = gestorCategorias.getById(p.getCategoriaBean().getIdCategorias());
				p.setCategoriaBean(c);
				gestorProductos.updateEntity(p);
			}
		}
		String resultString = msg.getString("productos.info.productSaved");
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPage();
	}

	public String addProducto(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		Producto p = new Producto();
		List<Categoria> categorias = gestorCategorias.findAll();
		List<Idioma> idiomas = gestorIdiomas.findAll();
		String resultString = "";
		if(categorias.size() > 0){
			resultString = msg.getString("productos.info.productAdded");
			p.setCategoriaBean(categorias.get(0));
			gestorProductos.saveEntity(p);
			//			Creamos tantos nombres vacios como idiomas hayan configurados
			for(Idioma i : idiomas){
				Nombre n = new Nombre();
				n.setIdioma(i);
				n.getProductos().add(p);
				gestorNombres.saveEntity(n);

				Descripcion d = new Descripcion();
				d.setIdioma(i);
				d.getProductos().add(p);
				gestorDescripciones.saveEntity(d);
			}


		}else{
			resultString = msg.getString("productos.error.noCategories");
		}
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPage();
	}

	public String eliminaProducto(Producto p){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("productos.info.productDeleted");
		p = gestorProductos.getById(p.getIdProductos());
		for(Descripcion desc : p.getDescripcions()){
			gestorDescripciones.deleteEntity(desc);
		}
		for(Nombre nombre : p.getNombres()){
			gestorNombres.deleteEntity(nombre);
		}
		gestorProductos.deleteEntity(p);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPage();
	}

	public String guardaProducto(){
		msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
		String resultString = msg.getString("detalleProducto.info.nombresProductosGuardados");
		gestorProductos.updateEntity(this.productoSeleccionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(resultString));
		return reloadPageProducto();
	}
	
    public void submitImage() throws IOException {
    	msg = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),  "#{msg}", ResourceBundle.class);
    	String resultString = msg.getString("detalleProducto.upload.correct");
    	int sizeLimit = 1024;
    	if(uploadedFile.getSize() < (sizeLimit * 1024)){//1MB
//    		Borramos la imagen anterior si existiera
    		if(!"".equals(productoSeleccionado.getImg())){
    			gestorProductos.eliminarImagenDeDisco(productoSeleccionado.getImg());
    		}
//    		Guardamos en BBDD
//    		productoSeleccionado.setImg(bytes);
//    		gestorProductos.updateEntity(productoSeleccionado);
//    		Guardamos en el filesystem
    		if(gestorProductos.guardarImgenEnDisco(uploadedFile)){
        		productoSeleccionado.setImg(uploadedFile.getFileName());
        		gestorProductos.updateEntity(productoSeleccionado);
    		}
    		
    		
    	}else{
    		resultString = msg.getString("detalleProducto.upload.maxSize") + sizeLimit + "KB";
    	}
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, msg.getString("detalleProducto.popup.titulo"),resultString));
    }


	public LazyDataModel<Producto> getListaProductos() {
		return listaProductos;
	}


	public void setListaProductos(LazyDataModel<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}
	
    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }


	public Producto getProductoSeleccionado() {
		return productoSeleccionado;
	}
	
	public List<Descripcion> getDescripcionesProducto() {
		return descripcionesProducto;
	}


	public void setDescripcionesProducto(List<Descripcion> descripcionesProducto) {
		this.descripcionesProducto = descripcionesProducto;
	}


	public List<Nombre> getNombresProducto() {
		return nombresProducto;
	}


	public void setNombresProducto(List<Nombre> nombresProducto) {
		this.nombresProducto = nombresProducto;
	}


	public void setProductoSeleccionado(Producto productoSeleccionado) {
		if(productoSeleccionado != null){
			this.productoSeleccionado = gestorProductos.getProductoTodosIdiomas(productoSeleccionado.getIdProductos());
			this.nombresProducto = new ArrayList<Nombre>(this.productoSeleccionado.getNombres());
			this.descripcionesProducto = new ArrayList<Descripcion>(this.productoSeleccionado.getDescripcions());	
		}
	}
	
	public void goToDetalleProducto() throws IOException{
		FacesContext.getCurrentInstance().getExternalContext().redirect("/views/admin/detalleProducto.xhtml");
	}

	public String reloadPageProducto(){
		setProductoSeleccionado(this.productoSeleccionado);
		initBean();
    	return "/views/admin/detalleProducto";
    }

	public String reloadPage(){
		initBean();
		return "/views/admin/productos";
	}

}
