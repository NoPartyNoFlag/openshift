package es.webapp.exiquets.beans;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.service.UsuariosService;

@Named
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ActivacionUsuarioBean {
	
	@Inject private UsuariosService gestorUsuarios;
	private FacesContext fCtx = FacesContext.getCurrentInstance();
	
	private ResourceBundle msg = fCtx.getApplication().evaluateExpressionGet(fCtx, "#{msg}", ResourceBundle.class);
	
	private String mailUsuario = null;
	private String idRegistro = null;
	private String newPassword = null;
	
	
	public String activaUsuario(){
		
		String resultado = msg.getString("login.info.userActivated");
		if(idRegistro != null && mailUsuario != null){
			User userFilter = new User();
			userFilter.setMail(mailUsuario);
			User user = gestorUsuarios.getUsuarioFiltrado(userFilter);
			if(user != null && idRegistro.equals(user.getPassword())){
				user.setEnabled(new Byte("1"));
				gestorUsuarios.updateEntity(user);
				resultado = msg.getString("login.info.userActivated");
			}else{
				resultado = msg.getString("login.info.userActivated") + ". idRegistro: " + idRegistro + " mailUsuario: " + mailUsuario;
			}
		}
		fCtx.addMessage(null, new FacesMessage(resultado));
		
		return "/views/login";
	}
	
	public String restorePassword(){
		String resultado = "";
		if(idRegistro != null && mailUsuario != null){
			User userFilter = new User();
			userFilter.setMail(mailUsuario);
			User user = gestorUsuarios.getUsuarioFiltrado(userFilter);
			if(user != null && idRegistro.equals(user.getPassword())){
				user.setPassword(newPassword);
				gestorUsuarios.updateEntity(user);
				resultado = msg.getString("login.info.passwordRestored");
			}else{
				resultado = msg.getString("login.error.passwordRestored");
			}
		}
		fCtx.addMessage(null, new FacesMessage(resultado));
		
		return "/views/login";
	}


	public String getMailUsuario() {
		return mailUsuario;
	}


	public void setMailUsuario(String mailUsuario) {
		this.mailUsuario = mailUsuario;
	}


	public String getIdRegistro() {
		return idRegistro;
	}


	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	

}
