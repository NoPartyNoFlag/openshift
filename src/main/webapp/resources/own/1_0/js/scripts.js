function encriptaValor(id){
	var node = $('[id$=' + id +' ]');
	var valor = node.val();
	if(valor == ''){
		return false;
	}
	var valorEncriptado = CryptoJS.MD5(valor);
	node.val(valorEncriptado);
}

$(document).ready(function(){
	var offset = new Date().getTimezoneOffset();
	var inputHidden = $("[id$='timeZoneOffset']").val(offset);
	
	if (typeof ContentFlowGlobal != 'undefined') {
	    new ContentFlowAddOn('ImageSelectAddOn', {
	        ContentFlowConf : {
	            // onclickInactiveItem: function (item) {
	            // this.conf.onclickActiveItem(item);
	            // },
	            onclickActiveItem : function(item) {
	                var content = item.content;
	                var i = item.index;
	                // console.log("index : "+item.index);
	                imageId = i + 1;
	                // console.log("select called image id : " + imageId);
	                rc([ {
	                    name : 'idProductoSeleccionado',
	                    value : imageId
	                } ]);
	            },
	            onReachTarget : function(item) {
//	                this.conf.onclickActiveItem(item);
	            }
	        }
	    });
	    ContentFlowGlobal.setAddOnConf("ImageSelectAddOn");
	}
});


window.fbAsyncInit = function() {
    FB.init({
      appId      : '1569766469944753',
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

