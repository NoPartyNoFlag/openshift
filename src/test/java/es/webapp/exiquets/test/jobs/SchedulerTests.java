package es.webapp.exiquets.test.jobs;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.webapp.exiquets.jobs.TaskScheduler;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/*Context.xml"})
public class SchedulerTests {
	private @Inject TaskScheduler programadorTareas;
	
	@Test
	public void deleteUsuariosAnonimosJobTest(){
		programadorTareas.eliminaUsuariosExpirados();
	}
	
	@Test
	public void deleteCarritosJobTest(){
		programadorTareas.eliminaCarritosExpirados();
	}
}
