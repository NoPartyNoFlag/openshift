package es.webapp.exiquets.test.repository;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.UserRole;
import es.webapp.exiquets.jobs.TaskScheduler;
import es.webapp.exiquets.repository.ProductosDAO;
import es.webapp.exiquets.repository.RolUsuarioDAO;
import es.webapp.exiquets.repository.UsuariosDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/*Context.xml"})
public class UsuariosTest {
	private UsuariosDAO usuariosDao;
	private ProductosDAO productosDao;
	private RolUsuarioDAO rolUsuariosDao;
	private ApplicationContext context; 
	
	@Before
	public void setUp(){
//		context = new ClassPathXmlApplicationContext("classpath:testContext.xml");
//		usuariosDao = (UsuariosDAO) context.getBean("usuarioDAO");
//		productosDao = (ProductosDAO) context.getBean("productoDAO");
//		rolUsuariosDao = (RolUsuarioDAO) context.getBean("rolUsuarioDAO");
	}
	
	@Test
	public void agregarProductos(){
		User u1 = usuariosDao.findAll().get(0);
		Producto p1 = productosDao.findAll().get(0);
		
		Set<Producto> productos = new HashSet<Producto>();
		productos.add(p1);
		
		u1.setProductos(productos);

		usuariosDao.update(u1);
	}
	
	
	@Test
	public void registrarUsuarioNuevo(){
		UserRole ur = new UserRole();
		ur.setAuthority("ROLE_USER");
		
		User u1 = new User();
		u1.setNombre("Ester");
		u1.setMail("fer2@mail.com");
		u1.setPassword("123456");
		u1.setEnabled(new Byte("1"));
		Set<UserRole> roles = new HashSet<UserRole>();
		roles.add(ur);
		u1.setUserRoles(roles);
		ur.setUser(u1);
		try{
			rolUsuariosDao.save(ur);
		}catch(PersistenceException e){
			System.out.println(e.getCause().getCause().getMessage());
		}
	}
	
	
}
