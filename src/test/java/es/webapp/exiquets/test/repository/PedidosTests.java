package es.webapp.exiquets.test.repository;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.webapp.exiquets.domain.entities.Pedido;
import es.webapp.exiquets.service.PedidosService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/*Context.xml"})
public class PedidosTests {

	@Inject private PedidosService gestorPedidos;
	
	@Test
	public void pedidoHasProductosTest(){
		gestorPedidos.getPedidoHasProductoCompletoById(2);
	}
	
	@Test
	public void getPedidoCompleto(){
		int rows = gestorPedidos.countPedidos(null);
		System.out.println(rows);
		List<Pedido> pedidos = gestorPedidos.getPedidos(0, 5, null, null, null);
		for(Pedido p : pedidos){
			System.out.println(p.getEstadoPedido().getIdEstadoPedido());
		}
	}
}
