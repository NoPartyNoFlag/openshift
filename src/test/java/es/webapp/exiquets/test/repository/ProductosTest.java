package es.webapp.exiquets.test.repository;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.webapp.exiquets.domain.entities.Nombre;
import es.webapp.exiquets.domain.entities.Nombrescategoria;
import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.domain.entities.User;
import es.webapp.exiquets.domain.entities.Usuariosanonimo;
import es.webapp.exiquets.repository.CategoriaDAO;
import es.webapp.exiquets.repository.ProductosDAO;
import es.webapp.exiquets.repository.UsuariosDAO;
import es.webapp.exiquets.service.ProductosService;
import es.webapp.exiquets.service.UsuariosAnonimosService;
import es.webapp.exiquets.service.UsuariosService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/*Context.xml"})
public class ProductosTest {
	@Inject private ProductosDAO productosDao;
	@Inject private UsuariosDAO usuariosDao;
	@Inject private CategoriaDAO categoriaDao;
	
	@Inject private ProductosService gestorProductos;
	@Inject private UsuariosService gestorUsuarios;
	@Inject private UsuariosAnonimosService gestorUsuariosAnonimos;
	
//	private ApplicationContext context;
	
	@Before
	public void setUp(){
//		context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");
//		productosDao = (ProductosDAO) context.getBean("productoDAO");
//		usuariosDao = (UsuariosDAO) context.getBean("usuarioDAO");
//		categoriaDao = (CategoriaDAO) context.getBean("categoriaDAO");
	}
	
	@Test
	public void agregarUsuario(){
		Producto p1 = productosDao.findAll().get(0);
		User u1 = usuariosDao.findAll().get(0);
		
		p1.getUsers().add(u1);
		
		productosDao.update(p1);
		
	}
	
	@Test
	public void updateProducto(){
		Producto p1 = productosDao.findAll().get(0);
		p1.setPrecio(75);
		
		productosDao.update(p1);
	}
	
	@Test
	public void registrarProductos(){
		Producto p1 = new Producto();
//		p1.setNombre("Oso de peluche");
		p1.setPrecio(25);
		p1.setImg(null);
		p1.setStock(5);
		
		productosDao.save(p1);
	}
	
	@Test
	public void registraProductoConCategoria(){
		Producto p1 = new Producto();
//		p1.setNombre("Oso de peluche");
		p1.setPrecio(25);
		p1.setImg(null);
		p1.setStock(5);
		
		p1.setCategoriaBean(categoriaDao.findAll().get(0));
		
		productosDao.update(p1);
	}
	
	@Test
	public void getAllProductos(){
		for(Producto p : productosDao.findAll()){
			System.out.println(p.getImg());
		}
	}
	
	
	private Usuariosanonimo getAnonymUser(){
		String idSesion = "5DFAC8617BD489AB73C84B047427A4EB";
		return gestorUsuariosAnonimos.getUsuarioPorSession(idSesion);
	}
}
