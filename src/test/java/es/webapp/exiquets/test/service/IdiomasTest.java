package es.webapp.exiquets.test.service;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.webapp.exiquets.service.IdiomasService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/*Context.xml"})
public class IdiomasTest {
	
	@Inject private transient IdiomasService gestorIdiomas;
	
	@Test
	public void testIdiomas(){
		
		gestorIdiomas.findAll();
		
	}

}
