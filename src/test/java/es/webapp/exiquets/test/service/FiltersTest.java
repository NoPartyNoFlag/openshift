package es.webapp.exiquets.test.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.primefaces.model.SortOrder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.webapp.exiquets.domain.entities.Producto;
import es.webapp.exiquets.service.ProductosService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/spring/*Context.xml"})
public class FiltersTest {
    
	@Inject ProductosService gestorProductos;

//    @Before
//    public void setUp() throws Exception {
//
//    }
    
	@Test
	public void namesFilter(){
		Map<String,Object> filters = new HashMap<String,Object>();
		
		filters.put("nombre", "ferrari");
		
		List<Producto> listaProductos = gestorProductos.getProductos(0, 5, null, SortOrder.ASCENDING, filters);
		
		System.out.println(listaProductos.size());
	}
	
	


}
